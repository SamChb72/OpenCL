/*******************************************************
*       Copyright (C) 2015-2030 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

#include "ofdm_fep.h"

__kernel void ofdm_fep(__global test* direction){
        int i = get_global_id(0);
        int j = get_global_id(1);
        int k = get_global_id(2);

        printf("GPU %d,%d,%d\n", i, j, k);	
}

