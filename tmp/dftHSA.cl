/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

/* Radix-2 DIT */
__kernel void dit2(__global short *x, __global short *y,
		   unsigned short int s, unsigned short int N)
{
    unsigned short int r = 1 << s;
    unsigned short int k = r << 1;
    unsigned short int i, l;
    short int Ar, Ai, Br, Bi;
    float a, xtw, ytw;
    i = get_global_id(0);
    if ((i % k) < r) {
	if (!i || k <= 4) {
	    xtw = 1;
	    ytw = 0;
	} else {
	    a = -2.0 * i / k;
	    xtw = cospi(a);
	    ytw = sinpi(a);
	}
	l = i + r;

	Ar = x[i] + x[l] * xtw - y[l] * ytw;
	Br = x[i] - x[l] * xtw + y[l] * ytw;
	Ai = y[i] + y[l] * xtw + x[l] * ytw;
	Bi = y[i] - y[l] * xtw - x[l] * ytw;

	x[i] = Ar;
	x[l] = Br;
	y[i] = Ai;
	y[l] = Bi;
    }
}

/* Radix-4 DIT */
__kernel void dit4(__global short *x, __global short *y,
		   unsigned short int s, unsigned short int N)
{
    unsigned short int r = 1 << (2 * s);
    unsigned short int n = r << 2;
    unsigned short int i, j, k, l;
    short int Ar, Ai, Br, Bi, Cr, Ci, Dr, Di;
    i = get_global_id(0);
//	printf("global_id=%d\n", i);
//	printf("local_id=%d\n", get_local_id(0));
    if ((i % n) < r) {
	j = i + r;
	k = j + r;
	l = k + r;

	Ar = x[i] + x[k] + x[j] + x[l];
	Ai = y[i] + y[k] + y[j] + y[l];
	Br = x[i] - x[k] + y[j] - y[l];
	Bi = y[i] - y[k] - x[j] + x[l];
	Cr = x[i] + x[k] - x[j] - x[l];
	Ci = y[i] + y[k] - y[j] - y[l];
	Dr = x[i] - x[k] - y[j] + y[l];
	Di = y[i] - y[k] + x[j] - x[l];

	x[i] = Ar;
	x[j] = Br;
	x[k] = Cr;
	x[l] = Dr;
	y[i] = Ai;
	y[j] = Bi;
	y[k] = Ci;
	y[l] = Di;
    }
}
