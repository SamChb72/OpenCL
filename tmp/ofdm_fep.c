/*******************************************************
*       Copyright (C) 2015-2030 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "ofdm_fep.h"

#define MAX_SOURCE_SIZE (0x100000)

#define NB_MEASURE 1
#define NANOINUSEC 1

#define DEBUG 1
#undef DEBUG

#define UINT16_SIZE 2

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

void print(const char *name, cl_short * v, unsigned int size)
{
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
	printf("%s[%d]=%d\n", name, i, v[i]);
    }
}

//bit reversing
/*
 * 0 000 -> 000 0 1 001 -> 100 4 2 010 -> 010 2 3 011 -> 110 6 4 100 -> 001 1
 * 5 101 -> 101 5 6 110 -> 011 3 7 111 -> 111 7
 */
unsigned char brev(int stages, unsigned char in)
{
    unsigned char out = 0;
    int j = 0;

    for (j = 0; j < stages; j++) {
	out += ((in & (1 << j)) >> j) << (stages - 1 - j);
    }
    return out;
}


int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 4;
    unsigned char stage = 3;
    int verbose = 0;
    int i = 0;
    int j = 0;
    int c;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;
    int radix = 4;

    //device type CPU or GPU(Default)
    int device_type = CL_DEVICE_TYPE_DEFAULT;

    cl_platform_id cpPlatform;
    cl_device_id device_id;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;

size=sizeof(test);
    //Allocate memory for each vector on host
    test *x = (test *) malloc(size);
	test *X = (test *)malloc(size);

    size_t global, local;
    cl_int err;

    FILE *fp;
    char *source_str, *include_str;
    size_t source_size, include_size;

    /* Load the source code containing the kernel */
    fp = fopen("./ofdm_fep.cl", "r");
    if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    /* Load the include file */
    const char *strings[1];
    strings[0] = source_str;
    size_t lenghts[1];
    lenghts[0] = source_size;

    //Bind to platform
    err = clGetPlatformIDs(1, &cpPlatform, NULL);

    //Get ID for the device
    err = clGetDeviceIDs(cpPlatform, device_type, 1, &device_id, NULL);

    //Create a context
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    //Create a command queue
#ifdef CL_VERSION_2_0
    queue =
	clCreateCommandQueueWithProperties(context, device_id, 0, &err);
#else
    queue = clCreateCommandQueue(context, device_id, 0, &err);
#endif

    //Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1,
					(const char **) &strings,
					(const size_t *) lenghts, &err);
    if (CL_SUCCESS != err)
	printf("create error = %d\n", err);
    //Build the program executable
    err |= clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (CL_SUCCESS != err)
	printf("build error = %d\n", err);

    //Create the compute kernel in the program we wish to run
	kernel = clCreateKernel(program, "ofdm_fep", &err);

    //Create the input and output arrays in device memory for our calculation
    cl_mem d_x =
	clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, NULL);

    //Set global and local workgroups size
    // Number of work items in each local work group
    local = 2;

    // Number of total work items - localSize must be devisor
    global = 6;



/*
    err |= clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(global), &global, NULL);
    err |= clGetKernelWorkGroupInfo(kernel, device_id,
			     CL_KERNEL_WORK_GROUP_SIZE, sizeof(local),
			     &local, NULL);
    if (CL_SUCCESS != err)
        printf("error = %d\n", err);

       local = 1;
*/


	printf("global=%d\n", (int)global);
	printf("local=%d\n", (int)local);

    int run = 0;
    memset(X, 0, size);

	//Initialize vectors
	memset(x, 0, size);
#if DEBUG
	print("x", x, size / UINT16_SIZE);
#endif
	    //Write our data set into the input array in device memory
	    err = clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0,
				       size, x, 0, NULL, NULL);
#if DEBUG
		print("x", x, size / UINT16_SIZE);
#endif
		//Set the arguments to our compute kernel
		err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_x);

		// Execute the kernel over the entire range of the data set
		err =
		    clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
					   &global, &local, 0, NULL, NULL);

		//Wait for the command queue to get serviced before reading back results
		clFinish(queue);
		//globalSize /= 4;

    clEnqueueReadBuffer(queue, d_x, CL_TRUE, 0, size, X, 0, NULL, NULL);

    //release OpenCL resources
    clReleaseMemObject(d_x);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    //release host memory
    free(x);

    return 0;
}
