/*******************************************************
*           Copyright (C) 2015 GreenFLOPS              *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

# bench
fetch sources:
git clone pi@greenflops-labs.com:/data/gitweb/bench.git


**************
   C Soft
**************
gcc -O3 -o dft dft.c -lm

***************
     HSA
***************

1/Convert the kernels:
snack.sh -q -c kernels.cl
snack.sh -vv -c -gccopt 3 fft.cl

2/Compile with hsa runtime
g++ -O3 -o benchHSA kernels.o benchHSA.cpp -L/opt/hsa/lib -lhsa-runtime64 -lelf
g++ -O3 -o fftHSA fft.o fftHSA.cpp -L/opt/hsa/lib -lhsa-runtime64 -lelf

USAGE & Examples:
benchHSA typeOfTest {-flops0, -flops1} cacheOption {nocache} vectorSize {xxx, xxxkB, xxxMB, xxxGB}
benchHSA -flops0 100kb
benchHSA -flops0 nocache 100kB
benchHSA -flops1 10MB
benchHSA -flops1 nocache 1GB


***************
    OpenCL
***************

Compile with:

gcc -O3 -o fftOpenCL fftOpenCL.c -L/opt/AMDAPPSDK-3.0/lib/x86_64/ -lOpenCL -lm
gcc -O3 -o benchOpenCL benchOpenCL.c -L/usrlib/fglrx -lOpenCL -lm

On MacOSX:
clang -framework OpenCL fftOpenCL.c -O3 -o fftOpenCL

Usage & examples:
benchOpenCL typeOfTest {-flops0, -flops1} cacheOption {nocache} vectorSize {xxx, xxxkB, xxxMB, xxxGB}
benchOpenCL -flops0 100kb
benchOpenCL -flops0 nocache 100kB
benchOpenCL -flops1 10MB
benchOpenCL -flops1 nocache 1GB

