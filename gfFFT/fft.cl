/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/


/* Radix-2 DIT */
__kernel void
dit2(__global float *x, __global float *y, unsigned short int s)
{
    unsigned int r = 1 << s;
    unsigned int k = r << 1;
    unsigned int i, j, l, num;
    float Ar, Ai, Br, Bi;
    float a, xtw, ytw, a2, xtw2, ytw2;
float Wr[2048], Wi[2048];

    i = get_global_id(0);

    if ((i % k) < r) {
	// stage 0 l = i+1 i+r
	// stage 1 l = i+2
	// stage 2 l = i+4
	l = i + r;
	a = -2.0f * (float) (i % k) / (float) (k);
	Wr[i] = cospi(a);
	Wi[i] = sinpi(a);

/*
	printf("i=%d\nW(%d, %d)=%3.3f + %3.3fi\n", i, i % k, k, Wr[i],
	       Wi[i]);

	a2 = -2.0f * M_PI * (float) (l % k) / (float) (k);
	Wr[l] = cos(a2);
	Wi[l] = sin(a2);

	printf("l=%d\nW(%d, %d)=%3.3f + %3.3fi\n", l, l % k, k, Wr[l],
	       Wi[l]);
*/

	// W[l] = -W[i] 
	Ar = x[i] + x[l] * Wr[i] - y[l] * Wi[i];
	Ai = y[i] + y[l] * Wr[i] + x[l] * Wi[i];
	Br = x[i] - x[l] * Wr[i] + y[l] * Wi[i];
	Bi = y[i] - y[l] * Wr[i] - x[l] * Wi[i];

	x[i] = Ar;
	y[i] = Ai;
	x[l] = Br;
	y[l] = Bi;
    }
}

/* Radix-4 DIT */
/*
__kernel void
dit4(__global float *x, __global float *y, unsigned short int s)
{
    unsigned short int r = 1 << (2 * s);
    unsigned short int n = r << 2;
    unsigned int i, j, k, l, num;
    float Ar, Ai, Br, Bi, Cr, Ci, Dr, Di;
    float ab, ac, ad;
    float Wbr[2048], Wbi[2048];
    float Wcr[2048], Wci[2048];
    float Wdr[2048], Wdi[2048];

    i = get_global_id(0);
    if ((i % n) < r) {
	j = i + r;
	k = j + r;
	l = k + r;

        ab = -2.0f * (float) (i % k) / (float) (k);
	ac = -2.0f * (float) (j % k) / (float) (k);
	ac = -2.0f * (float) (k % k) / (float) (k);
        Wbr[i] = cospi(ab);
        Wbi[i] = sinpi(ab);
	Wcr[i] = cospi(ac);
        Wci[i] = sinpi(ac);


	Xr = Ar + (Cr * Wcr - Ci * Wci) + (Br * Wbr - Bi * Wbi) + (Dr * Wdr - Di * Wdi);
	Xi = Ai + (Cr * Wci + Ci * Wcr) + (Br * Wbi + Bi * Wbr) + (Dr * Wdi + Di * Wdr);
	Yr = Ar - (Cr * Wcr - Ci * Wci) + (Br * Wbi + Bi * Wbr) + (Dr * Wdi + Di * Wdr);
	Yi = Ai - (Cr * Wci + Ci * Wcr) - (Br * Wbr - Bi * Wbi) + (Dr * Wdr - Di * Wdi);
	Zr = Ar + (Cr * Wcr - Ci * Wci) - (Br * Wbr - Bi * Wbi) - (Dr * Wdr - Di * Wdi);
	Zi = Ai + (Cr * Wci + Ci * Wcr) - (Br * Wbi - Bi * Wbr) - (Dr * Wdi - Di * Wdr);
	Vr = Ar - (Cr * Wcr - Ci * Wci) - (Br * Wbi + Bi * Wbr) + (Dr * Wdi + Di * Wdr);
	Vi = Ai - (Cr * Wci + Ci * Wcr) + (Br * Wbr - Bi * Wbi) - (Dr * Wdr - Di * Wdi);

	Ar = x[i] + x[k] + x[j] + x[l];
	Ai = y[i] + y[k] + y[j] + y[l];
	Br = x[i] - x[k] + y[j] - y[l];
	Bi = y[i] - y[k] - x[j] + x[l];
	Cr = x[i] + x[k] - x[j] - x[l];
	Ci = y[i] + y[k] - y[j] - y[l];
	Dr = x[i] - x[k] - y[j] + y[l];
	Di = y[i] - y[k] + x[j] - x[l];

	x[i] = Ar;
	x[j] = Br;
	x[k] = Cr;
	x[l] = Dr;
	y[i] = Ai;
	y[j] = Bi;
	y[k] = Ci;
	y[l] = Di;
    }
}
*/
