/*******************************************************
*       Copyright (C) 2015 GreenFLOPS	       *
*						       *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*						       *
*              contact@greenflops.com		       *
*******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>

#define NB_MEASURE 1000

#define DEBUG 1
#undef DEBUG

#define SIZE_BYTES 4

typedef unsigned char uint8_t;
unsigned int num;
float Wr[14 * 2048], Wi[14 * 2048];

void print(const char *name, float *v, float *w, unsigned int size);

void dit2(float *x, float *y, uint8_t s, unsigned int N, float number)
{
    unsigned int r = 1 << s;
    unsigned int k = r << 1;
    unsigned int i, j, l, num;
    float Ar, Ai, Br, Bi;
    float a, xtw, ytw, a2, xtw2, ytw2;

    //printf("stage%d\n", s);
    for (num = 0; num < number; num++) {
	for (i = N * num; i < N * (num + 1); i += 1) {
	    if (((i - N * num) % k) < r) {
		// stage 0 l = i+1 i+r
		// stage 1 l = i+2
		// stage 2 l = i+4
		l = i + r;
		a = -2.0f * M_PI * (float) (i % k) / (float) (k);
		Wr[i] = cos(a);
		Wi[i] = sin(a);
//                      printf("i=%d\nW(%d, %d)=%3.3f + %3.3fi\n", i, i%k, k, Wr[i], Wi[i]);
/*		
			a2 = -2.0f * M_PI * (float)(l%k) / (float)(k);
			Wr[l] = cos(a2);
			Wi[l] = sin(a2);
*/
//                      printf("l=%d\nW(%d, %d)=%3.3f + %3.3fi\n", l, l%k, k, Wr[l], Wi[l]);

		// W[l] = -W[i] 
		Ar = x[i] + x[l] * Wr[i] - y[l] * Wi[i];
		Ai = y[i] + y[l] * Wr[i] + x[l] * Wi[i];
		Br = x[i] - x[l] * Wr[i] + y[l] * Wi[i];
		Bi = y[i] - y[l] * Wr[i] - x[l] * Wi[i];

		x[i] = Ar;
		y[i] = Ai;
		x[l] = Br;
		y[l] = Bi;
	    }
	}
    }
}

void print(const char *name, float *v, float *w, unsigned int size)
{
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
	printf("%s[%d]=%3.3f, %3.3f\n", name, i, v[i], w[i]);
    }
}

//bit reversing
/*
 * 0 000 -> 000 0 1 001 -> 100 4 2 010 -> 010 2 3 011 -> 110 6 4 100 -> 001 1
 * 5 101 -> 101 5 6 110 -> 011 3 7 111 -> 111 7
 */
unsigned int brev(unsigned char stages, unsigned int in)
{
    unsigned int out = 0;
    unsigned int j = 0;

    for (j = 0; j < stages; j++) {
	out += ((in & (1 << j)) >> j) << (stages - 1 - j);
    }
    return out;
}


int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    float meas;
    float mean = 0;
    float var = 0;
    unsigned int size = 1;
    unsigned char stage = 3;
    int verbose = 0;
    int i = 0;
    int j = 0;
    int c;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;
    int radix = 4;
    unsigned int number = 1;
    unsigned int length;
    unsigned int total_length;
    /* Input vectors */
    float *x;
    float *y;
    float *X;
    float *Y;
    float *tmp;
    float *tmp2;

    if (4 > argc) {
	printf("Usage & exemples:\n");
	printf("./fft radix stages\n");
	printf
	    ("./fft 2 5 3 to compute radix-2 fft in 5 stages (32 elem), 3 vectors of 32\n");
	radix = 2;
	stage = 5;
	length = pow(radix, stage);
	number = 3;
    } else {
	radix = atoi(argv[pos++]);
	stage = atoi(argv[pos++]);
	number = atoi(argv[pos++]);
	length = pow(radix, stage);
    }
    num = number;
    bzero(Wr, 14 * 2048);
    bzero(Wi, 14 * 2048);
    size = length * SIZE_BYTES;

    printf("length = %d, total length = %d\n", length, number * length);

    x = (float *) malloc(number * size);
    X = (float *) malloc(number * size);
    y = (float *) malloc(number * size);
    Y = (float *) malloc(number * size);
    tmp = (float *) malloc(number * size);
    tmp2 = (float *) malloc(number * size);
    memset(X, 0, number * size);
    memset(Y, 0, number * size);

    int run;
    for (run = -100; run < NB_MEASURE; run++) {
	if (run == 0)
	    gettimeofday(&tstart, NULL);
	memset(x, 0, number * size);
	memset(y, 0, number * size);
	memset(tmp, 0, number * size);
	memset(tmp2, 0, number * size);
	for (j = 0; j < number; j++) {
	    for (i = 0; i < length; i++) {
		x[i + j * length] = 2 * (i);
		y[i + j * length] = 2 * (i) + 1.0f;
	    }
	}

	//print("in", x, y, 16);        //number * size / SIZE_BYTES);

	for (j = 0; j < number; j++) {
	    int k = 0;
	    for (i = 0; i < length; i++) {
		k = brev(stage, i);
		tmp[i + j * length] = x[j * length + k];
		tmp2[i + j * length] = y[j * length + k];
	    }
	}

	int nbfft = 0;
	memcpy(x, tmp, number * size);
	memcpy(y, tmp2, number * size);
	//print("in", x, y, 16);
	for (i = 0; i < stage; i++) {
#if DEBUG
	    printf("Stage %d:\n", i + 1);
	    print("in", x, y, 10);	//number * size);
#endif
	    dit2(x, y, i, length, number);
	}
    }
    memcpy(X, x, number * size);
    memcpy(Y, y, number * size);
    gettimeofday(&tstop, NULL);
    meas =
	((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) -
	tstart.tv_usec;
    printf("%d, %f\n", length, meas / (float) NB_MEASURE);

//#ifdef DEBUG
    //print("out", X, Y, 16);
//#endif
    free(x);
    free(y);
    free(X);
    free(Y);
    free(tmp);
    free(tmp2);

    return 0;
}
