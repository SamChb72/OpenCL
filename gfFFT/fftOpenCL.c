/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifndef _WIN32
#include <sys/time.h>
#include <getopt.h>
#endif
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

#define NB_MEASURE 1000

#define DEBUG 1
#undef DEBUG

#define FLOAT_SIZE 4

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

void print(const char *name, cl_float * v, cl_float * w, unsigned int size)
{
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
	printf("%s[%d]=%3.3f, %3.3f\n", name, i, v[i], w[i]);
    }
}

//bit reversing
/*
 * 0 000 -> 000 0 1 001 -> 100 4 2 010 -> 010 2 3 011 -> 110 6 4 100 -> 001 1
 * 5 101 -> 101 5 6 110 -> 011 3 7 111 -> 111 7
 */
unsigned char brev(int stages, unsigned char in)
{
    unsigned char out = 0;
    int j = 0;

    for (j = 0; j < stages; j++) {
	out += ((in & (1 << j)) >> j) << (stages - 1 - j);
    }
    return out;
}


int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    float meas;
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 4;
    unsigned char stage = 3;
    int verbose = 0;
    int i = 0;
    int j = 0;
    int c;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;
    int radix = 4;
    int number = 1;
    unsigned int length = 1;
    unsigned int total_length;
    int zoom = 2;

    //device type CPU or GPU(Default)
    int device_type = CL_DEVICE_TYPE_GPU;

    cl_platform_id cpPlatform;
    cl_device_id device_id;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;

    if (4 > argc) {
	printf("Usage & exemples:\n");
	printf("./fftOpenCL radix stages\n");
	printf
	    ("./fftOpenCL 4 5 2 to compute the fft radix 4 algo in 5 stages (1024 elem) for 4x4 = 16 lengths\n");
	radix = 4;
	stage = 5;
	zoom = 2;
	length = pow(radix, stage);
	exit(1);
    } else {
	radix = atoi(argv[pos++]);
	stage = atoi(argv[pos++]);
	zoom = atoi(argv[pos++]);
	if (2 == radix)
	    length = pow(2, stage);
	else if (4 == radix)
	    length = pow(4, stage);
	printf("%d stages => N=%d\n", stage, length);
    }
    total_length = length;
    for (i = 0; i < zoom; i++) {
	total_length *= radix;
    }
    number = total_length / length;
    size = total_length * FLOAT_SIZE;

    printf("length = %d, total length = %d\n", length, total_length);

    //Allocate memory for each vector on host
    cl_float *x = (cl_float *) malloc(size);
    cl_float *X = (cl_float *) malloc(size);
    cl_float *y = (cl_float *) malloc(size);
    cl_float *Y = (cl_float *) malloc(size);
    cl_float *tmp = (cl_float *) malloc(size);
    cl_float *tmp2 = (cl_float *) malloc(size);

    size_t global, local;
    cl_int err;

    FILE *fp;
    char *source_str, *include_str;
    size_t source_size, include_size;

    /* Load the source code containing the kernel */
    fp = fopen("./fft.cl", "r");
    if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    /* Load the include file */
    const char *strings[1];
    strings[0] = source_str;
    size_t lenghts[1];
    lenghts[0] = source_size;

    //Bind to platform
    err = clGetPlatformIDs(1, &cpPlatform, NULL);

    //Get ID for the device
    err = clGetDeviceIDs(cpPlatform, device_type, 1, &device_id, NULL);

    //Create a context
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    //Create a command queue
//#ifdef CL_VERSION_2_0
//    queue =
//      clCreateCommandQueueWithProperties(context, device_id, 0, &err);
//#else
    queue = clCreateCommandQueue(context, device_id, 0, &err);
//#endif

    //Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1,
					(const char **) &strings,
					(const size_t *) lenghts, &err);
    if (CL_SUCCESS != err)
	printf("error = %d\n", err);
    char options[] = "-cl-mad-enable";
    //Build the program executable
    err |= clBuildProgram(program, 0, NULL, options, NULL, NULL);

    if (CL_SUCCESS != err)
	printf("error = %d\n", err);

    //Create the compute kernel in the program we wish to run
    if (2 == radix)
	kernel = clCreateKernel(program, "dit2", &err);
    else if (4 == radix)
	kernel = clCreateKernel(program, "dit4", &err);

    //Create the input and output arrays in device memory for our calculation
    cl_mem d_x =
	clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, NULL);
    cl_mem d_y =
	clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, NULL);

    global = length;

    printf("global=%d\n", (int) global);

    int run = 0;
    memset(X, 0, size);
    memset(Y, 0, size);

    for (run = -100; run < NB_MEASURE; run++) {
	if (run == 0) gettimeofday(&tstart, NULL);
	//Initialize vectors
	memset(x, 0, size);
	memset(y, 0, size);
	memset(tmp, 0, size);
	memset(tmp2, 0, size);

	for (j = 0; j < number; j++) {
	    for (i = 0; i < length; i++) {
		x[i + j * length] = 2 * (i);
		y[i + j * length] = 2 * (i) + 1.0f;
	    }
	}

	for (j = 0; j < number; j++) {
	    int k = 0;
	    for (i = 0; i < length; i++) {
		k = brev(stage, i);
		tmp[i + j * length] = x[j * length + k];
		tmp2[i + j * length] = y[j * length + k];
	    }
	}

	int nbfft;
	memcpy(x, tmp, size);
	memcpy(y, tmp2, size);

	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_x);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_y);

	for (i = 0; i < stage; i++) {

	err = clEnqueueWriteBuffer(queue, d_x, CL_FALSE, 0, size, x, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(queue, d_y, CL_FALSE, 0, size, y, 0, NULL, NULL);
	    //Set the arguments to our compute kernel
	    err |= clSetKernelArg(kernel, 2, sizeof(short), &i);

	    // Execute the kernel over the entire range of the data set
	    err =
		clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
				       &global, NULL, 0, NULL, NULL);

	
cl_event readevent;
    clEnqueueReadBuffer(queue, d_x, CL_TRUE, 0, size, x, 0, NULL, &readevent);
    clEnqueueReadBuffer(queue, d_y, CL_TRUE, 0, size, y, 0, NULL, &readevent);
clWaitForEvents (1, &readevent);
	}
	memcpy(X, x, size);
	memcpy(Y, y, size);
    }

    gettimeofday(&tstop, NULL);
    meas =
	((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) -
	tstart.tv_usec;
    printf("%d, %f\n", length, meas / (float) NB_MEASURE);

print("out", X, Y, 16);

    //release OpenCL resources
    clReleaseMemObject(d_x);
    clReleaseMemObject(d_y);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    //release host memory
    free(x);
    free(y);
    free(X);
    free(Y);
    free(tmp);
    free(tmp2);

    return 0;
}
