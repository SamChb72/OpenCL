#/*******************************************************
#*       Copyright (C) 2015 GreenFLOPS             *
#*                                                      *
#* This file is part of GreenFLOPS projects and can not *
#* be copied and/or distributed in any medium or format *
#*    without the express permission of GreenFLOPS      *
#*                                                      *
#*              contact@greenflops.com                  *
#*******************************************************/

#! /bin/bash

# Get hardware information on current node

#Network
echo "--------------------"
echo "      Network       "
echo "--------------------"
echo ""
ifconfig |grep HWaddr
echo ""

# CPU 
echo "--------------------"
echo "        CPU         "
echo "--------------------"
echo ""
lscpu
echo ""
cat /proc/cpuinfo
echo ""

# Memory
echo "--------------------"
echo "      Memory        "
echo "--------------------"
echo ""
free
echo ""

# Hard Disk
echo "--------------------"
echo "     Hard Disk      "
echo "--------------------"
echo ""
df -h | grep /dev
echo ""


# OpenCL
echo "--------------------"
echo "       OpenCL       "
echo "--------------------"
echo ""
clinfo

