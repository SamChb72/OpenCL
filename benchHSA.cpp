/*******************************************************
*           Copyright (C) 2015 GreenFLOPS              *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <libelf.h>
#include <iostream>
#include <sys/time.h>
#include <getopt.h>
#include "kernels.h"

#define NB_MEASURE 100
#define NANOINUSEC 1000
#define WARMUP 100

enum TEST { FLOPS_0, FLOPS_1 };

int main(int argc, char **argv)
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 1;
    int verbose = 0;
    int i = 0;
    int c;
    int warmup = WARMUP;
    int coef = 1;
    int pos = 1;
    int start = 0;
    TEST test = FLOPS_0;

    if (2 > argc) {
	printf("Usage & exemples:\n");
	printf
	    ("benchHSA typeOfTest {-flops0, -flops1} cacheOption {nocache} vectorSize {xxx, xxxkB, xxxMB, xxxGB}\n");
	printf("benchHSA -flops0 100kb\n");
	printf("benchHSA -flops0 nocache 100kB\n");
	printf("benchHSA -flops1 10MB\n");
	printf("benchHSA -flops1 nocache 1GB\n");
	printf("\n");
	printf("\n");
	printf("Run default case ./benchHSA -flops 1\n");

    } else {
	if ('-' == argv[pos][0]) {
	    if (!strcmp(argv[pos], "-flops0"))
		test = FLOPS_0;
	    else if (!strcmp(argv[pos], "-flops1"))
		test = FLOPS_1;
	    pos++;
	}
	if (!strcmp("warmup", argv[pos])) {
	    pos++;
	    warmup = atoi(argv[pos++]);
	}
	coef = 1;
	if (NULL != strcasestr(argv[pos], "kB"))
	    coef = 1024;
	else if (NULL != strcasestr(argv[pos], "MB"))
	    coef = 1024 * 1024;
	else if (NULL != strcasestr(argv[pos], "GB"))
	    coef = 1024 * 1024 * 1024;
	size = coef * atoi(argv[pos]);
    }
    if (1 == coef) {
	size *= sizeof(short);
    }
    //Setup kernel arguments
    short *in = (short *) malloc(size);
    short *out = (short *) malloc(size);
    memset(in, 0, size);
    memset(out, 0, size);

    for (i = 0; i < (size / sizeof(short)); i++)
	in[i] = 1;//(short) i;

    SNK_INIT_LPARM(lparm, size / sizeof(short));
	int run;
    for (run = -100; run < NB_MEASURE; run++) {
        int nbtest = 0;
        if (run >= 0)
            gettimeofday(&tstart, NULL);
        for (nbtest = 0; nbtest < NANOINUSEC; nbtest++) {
                if(0 == test) flop_0(in, out, lparm);
                else if (1 == test) flop_1(in, lparm);
        }
        if (run >= 0) {
            gettimeofday(&tstop, NULL);
            meas[run] =
                ((tstop.tv_sec - tstart.tv_sec) * 1000000L +
                 tstop.tv_usec) - tstart.tv_usec;
                meas[run];
        }
    }

    for (run = 0; run < NB_MEASURE; run++) {
        mean += meas[run];
    }

    mean /= NB_MEASURE;
    for (run = 0; run < NB_MEASURE; run++) {
        var += ((meas[run] - mean) * (meas[run] - mean));
    }
    var /= NB_MEASURE;


/*
    for (i = -warmup; i < NB_MEASURE; i++) {
	int nbtest = 0;
	if (FLOPS_0 == test) {
	    if (0 <= i)		// Start measurement after WARMUP
		gettimeofday(&tstart, NULL);
	    flop_0(in, out, lparm);
	} else if (FLOPS_1 == test) {
	    if (0 <= i)
		gettimeofday(&tstart, NULL);
	    flop_1(in, lparm);
	}
	gettimeofday(&tstop, NULL);
	if (0 <= i)
	    meas[i] =
		((tstop.tv_sec - tstart.tv_sec) * 1000000L +
		 tstop.tv_usec) - tstart.tv_usec;
    }
    for (i = 0; i < NB_MEASURE; i++) {
	mean += meas[i];
    }
    mean /= NB_MEASURE;
    for (i = 0; i < NB_MEASURE; i++) {
	var += ((meas[i] - mean) * (meas[i] - mean));
    }
    var /= NB_MEASURE;
*/

    if (FLOPS_0 == test) {
#if 0
	printf
	    ("HSA: Vector of %lu shorts of %d-bytes = %u Bytes takes %lu usec [+/-var %lu] = Copy Speed = %.3f GByte/s\n",
	     (size / sizeof(short)), (int) sizeof(short), size, mean,
	     var, (short) size / (short) mean);
#endif
	printf("%lu,%u,%lu,%lu,%.3f\n", size / sizeof(short),
	       size, mean, var, 1000000.0f / (short) mean);
    } else if (FLOPS_1 == test) {
#if 0
	printf
	    ("HSA: Vector of %lu shorts of %d-bytes = %u Bytes takes %lu usec [+/-var %lu] => FLOPS Speed = %.3f Gflops\n",
	     (size / sizeof(short)), (int) sizeof(short), size, mean,
	     var, (short) (1.0f * size / sizeof(short) / mean));
#endif
	printf("%lu,%u,%lu,%lu,%.3f\n",
	       size / sizeof(short), size, mean, var,
	       (1.0f * size / sizeof(short) / mean));
    }
    //Validate
/*
    bool valid = true;
    int failIndex = 0;
    if (FLOPS_0 == test) {
	for (i = 0; i < (size / sizeof(short)); i++) {
	    if (verbose && i < 10)
		printf("out[%d]=%f, ", i, out[i]);
	    if (out[i] != in[i]) {
		failIndex = i;
		valid = false;
		break;
	    }
	}
	if (valid) {
	    if (verbose)
		printf("passed validation\n");
	} else
	    printf("VALIDATION FAILED!\nBad index: %d\n", failIndex);
    } else if (FLOPS_1 == test) {
	for (i = 0; i < (size / sizeof(short)); i++) {
	    if (verbose)
		printf("out[%d]=%.1f", i, out[i]);
	    if (out[i] != 555.0f - (short) i) {
		failIndex = i;
		valid = false;
		break;
	    }
	}
	if (valid) {
	    if (verbose)
		printf("passed validation\n");
	} else
	    printf("VALIDATION FAILED!\nBad index: %d\n", failIndex);
    }
*/
    free(in);
    free(out);
    return 0;
}
