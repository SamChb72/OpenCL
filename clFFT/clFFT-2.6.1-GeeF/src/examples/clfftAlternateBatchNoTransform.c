/* ************************************************************************
 * Copyright 2013 Advanced Micro Devices, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <rdtsc.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

/* No need to explicitely include the OpenCL headers */
#include <clFFT.h>

#ifdef __APPLE__
#define CLOCK_PROCESS_CPUTIME_ID 1
//clock_gettime is not implemented on OSX
int clock_gettime(int clk_id, struct timespec *t)
{
    struct timeval now;
    int rv = gettimeofday(&now, NULL);

    if (rv)
	return rv;
    t->tv_sec = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}
#endif

int main(int argc, char *argv[])
{
    cl_int err;
    cl_platform_id platform[32];
    int platformId = 0;
    cl_device_id device = 0;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_context ctx = 0;
    cl_command_queue queue = 0;
    cl_mem bufX;
    float *X, *Y, *X2;
    int ret = 0;
    size_t N = 16;
    size_t size = 16;
    char platform_name[128];
    char platform_version[128];
    char device_name[128];
    int verbose = 0;
    int nb_loop = 1000;
    int run = 0;
    int warmup = 0;
    int dir = 1;
    int i;
    int batchSize = 1;
    bzero(platform_name, 128);
    bzero(platform_version, 128);
    bzero(device_name, 128);
    cl_device_type device_type = CL_DEVICE_TYPE_GPU;
    /* FFT library realted declarations */
    clfftPlanHandle planHandle;
    clfftDim dim = CLFFT_1D;
    char amd_platform[] = "AMD"; // 1
    char intel_platform[] = "Intel"; // 0
    char *user_platform;
    user_platform = amd_platform;
    size_t clInStrides[1], clOutStrides[0];
    clInStrides[0] = 1;
    clOutStrides[0] = 1;
    //size_t clLengths[1] = {N};

    if (10 > argc) {
	   printf("Usage & exemples:\n");
	   printf("%s $verbosityCallLevel $oclPlatform $oclVersion $oclDevice $direction $N $nbWarmup $nbLoop $batchSize\n", argv[0]);
	   printf("./fft1dAlternate [0..2] [AMD|Intel] [1.2|2.0] [CPU|GPU] [-1 1] 1024 -100 1000 1\n");
	   exit(0);
    } else {
	    verbose = atoi(argv[1]);
        user_platform = argv[2];
	    //if(!strcasecmp("Intel", argv[2])) user_platform = intel_platform;
	    //else if(!strcasecmp("AMD", argv[2])) user_platform = amd_platform;
	    if (!strcasecmp("CPU", argv[4])) device_type = CL_DEVICE_TYPE_CPU;
	    else if (!strcasecmp("GPU", argv[4])) device_type = CL_DEVICE_TYPE_GPU;
	    dir = atoi(argv[5]);
	    N = atoi(argv[6]);
	    warmup = atoi(argv[7]);
	    nb_loop = atoi(argv[8]);
        batchSize = atoi(argv[9]);
    }
    //printf("%s\n", user_platform);
    clfftDirection direction = CLFFT_FORWARD;
    if (-1 == dir) {
	   direction = CLFFT_BACKWARD;
	//printf("Inverse FFT\n");
    } /*else
	printf("Forward FFT\n");
      */
    size_t clLengths[1] = { N };

    /* Setup OpenCL environment. */
    cl_uint num_platforms = 0;
      size_t ret_param_size = 0;
    err = clGetPlatformIDs(32, platform, &num_platforms);
    //printf("%d platforms found\n", num_platforms);
 
    for(i=0; i<num_platforms; i++){
      err = clGetPlatformInfo(platform[i], CL_PLATFORM_NAME, sizeof(platform_name), platform_name, &ret_param_size);
      //printf("Platform found: %s\n", platform_name);
      if(NULL != strcasestr(platform_name, user_platform)){
	platformId = i;
	break;
      };
    }
    //printf("platform Id = %d\n", platformId);

   err = clGetPlatformInfo(platform[i], CL_PLATFORM_VERSION, sizeof(platform_version), platform_version, &ret_param_size);

   err = clGetDeviceIDs(platform[platformId], device_type, 1, &device, NULL);

    err = clGetDeviceInfo(device, CL_DEVICE_NAME,
			  sizeof(device_name), device_name,
			  &ret_param_size);
    if(verbose>0)printf("platform=[%s], version=[%s], Device=[%s]\n", platform_name, platform_version, device_name);
    if(!device){
        printf("No device\n");
        return 0;
    }
    props[1] = (cl_context_properties) platform[platformId];
    ctx = clCreateContext(props, 1, &device, NULL, NULL, &err);
    queue =
	clCreateCommandQueue(ctx, device, CL_QUEUE_PROFILING_ENABLE, &err);

    /* Setup clFFT. */
    clfftSetupData fftSetup;
    err = clfftInitSetupData(&fftSetup);
    err = clfftSetup(&fftSetup);

    /* Allocate host & initialize data. */
    /* Only allocation shown for simplicity. */
    X = (float *) calloc(N * 2 * batchSize, sizeof(float));
    X2 = (float *) calloc(N * 2 * batchSize, sizeof(float));
    Y = (float *) calloc(N * 2 * batchSize, sizeof(float));

    /* print input array */
    int print_iter = 0;

    if(1 == batchSize){
        for (print_iter=0; print_iter < 2 * N; print_iter++) {
            X[print_iter] = print_iter;
            X[print_iter + 1] = print_iter + 1;
       }
    }
    else{ // interlace even number
        int job;
        for (job=0; job < batchSize; job++) {
            for (print_iter=0; print_iter < 2*N; print_iter+=2) {
                X[job * 2 * N + print_iter] = print_iter;
                X[job * 2 * N + print_iter + 1] = print_iter + 1;
            }
        }
    }

    if (2 == verbose) {
        if(1 == batchSize){
            printf("Input1 = \n");
            for (i = 0; i < 2*N; i += 2)
                printf("%f+%fi;", X[i], X[i + 1]);
            printf("\n");
	        printf("Input2 = \n");
	        for (i = 0; i < 2*N; i += 2)
                printf("%f+%fi;", X2[i], X2[i + 1]);
	        printf("\n");
        }
        else{
            printf("input = \n");
            for (i = 0; i < 2*N*batchSize; i += 2)
                printf("%f+%fi;", X[i], X[i + 1]);
            printf("\n");
        }
    }

    /* Create a default plan for a complex FFT. */
    err = clfftCreateDefaultPlan(&planHandle, ctx, dim, clLengths);

    /* Set plan parameters. */
    err = clfftSetPlanPrecision(planHandle, CLFFT_SINGLE);
    err =
	clfftSetLayout(planHandle, CLFFT_COMPLEX_INTERLEAVED,
		       CLFFT_COMPLEX_INTERLEAVED);
    err = clfftSetResultLocation(planHandle, CLFFT_INPLACE);

    /* Set batchSize */
    err = clfftSetPlanBatchSize(planHandle, batchSize);
    err = clfftSetPlanDistance(planHandle, N, N);
    /*
    err = clfftSetPlanInStride(planHandle, dim, clInStrides);
    err = clfftSetPlanOutStride(planHandle, dim, clOutStrides);
    */

    /* Bake the plan. */
    err = clfftBakePlan(planHandle, 1, &queue, NULL, NULL);

    /* Prepare OpenCL memory objects and place data inside them. */
    bufX =
	clCreateBuffer(ctx, CL_MEM_READ_WRITE, N * 2 * batchSize * sizeof(*X), NULL,
		       &err);

    {
	struct timeval utstart, utstop;
	double uduration;

	for (run = warmup; run < nb_loop; run++) {
	    if (0 == run) {
		gettimeofday(&utstart, NULL);
	    }
	    //if (0 == run % 2)
		err = clEnqueueWriteBuffer(queue, bufX, CL_FALSE, 0,
					   N * 2 * batchSize * sizeof(*X), X, 0, NULL,
					   NULL);
        /*
	    else
		err =
		    clEnqueueWriteBuffer(queue, bufX, CL_FALSE, 0,
					 N * 2 * batchSize * sizeof(*X2), X2, 0, NULL,
					 NULL);
        */
#if 0
//NO TRANSFORM
	    err =
		clfftEnqueueTransform(planHandle, direction, 1, &queue,
				      0, NULL, NULL /*event */ , &bufX,
				      NULL, NULL);

	    /* Wait for calculations to be finished. */
	    err = clFinish(queue);
#endif
	    /* Fetch results of calculations. */
    	    /*
	    Y = clEnqueueMapBuffer(queue, bufX, CL_TRUE, CL_MAP_READ, 0,
				   N * 2 * batchSize * sizeof(*Y), 0, NULL, NULL,
				   &err);
                   */
	err =
	  clEnqueueReadBuffer (queue, bufX, CL_TRUE, 0,
			       N * 2 * batchSize * sizeof (*Y), Y, 0, NULL, NULL);
/*
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  	total_time = time_end - time_start;
  	printf("Kernel exec event profiling [ns] = %9.9f \n", (total_time));
*/
	}
	gettimeofday(&utstop, NULL);
	uduration = ((utstop.tv_sec - utstart.tv_sec) * 1000000L + utstop.tv_usec) - utstart.tv_usec;

	if (2 == verbose) {
	    print_iter = 0;
	    printf("Output%d = \n", run);
	    while (print_iter < 2 * N * batchSize) {
		printf("%f+%fi,", Y[print_iter], Y[print_iter + 1]);
		print_iter += 2;
	    }
	    printf("\n");
	}

	printf("%d, %f\n", N, uduration / batchSize / nb_loop);

    }
    /* Release OpenCL memory objects. */
    clReleaseMemObject(bufX);

    free(X);
    free(X2);

    /* Release the plan. */
    err = clfftDestroyPlan(&planHandle);
    /* Release clFFT library. */
    clfftTeardown();
    /* Release OpenCL working objects. */
    clReleaseCommandQueue(queue);
    clReleaseContext(ctx);
    return ret;
}
