/*******************************************************
*       Copyright (C) 2015 GreenFLOPS	       *
*						       *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*						       *
*              contact@greenflops.com		       *
*******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <getopt.h>

#define NB_MEASURE 1000
#define L1_SIZE (2*128*1024)
#define L2_SIZE (2*2048*1024)


int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    int *x = (int *) calloc(L1_SIZE, sizeof(int));
    int *y = (int *) calloc(L1_SIZE, sizeof(int));
    unsigned long int meas;
    int run = 0;
    for (run = -100; run < NB_MEASURE; run++) {
	if (0 == run)
	    gettimeofday(&tstart, NULL);
	memcpy(x, y, L1_SIZE);
    }
    gettimeofday(&tstop, NULL);
    meas =
	((tstop.tv_sec - tstart.tv_sec) * 1000000L +
	 tstop.tv_usec) - tstart.tv_usec;

    printf("L1: %.3f GByte/sec\n",
	   (double) L1_SIZE * NB_MEASURE / meas / 1000);

    free(x);
    free(y);

    x = (int *) calloc(L2_SIZE, sizeof(int));
    y = (int *) calloc(L2_SIZE, sizeof(int));

    for (run = -100; run < NB_MEASURE; run++) {
	if (0 == run)
	    gettimeofday(&tstart, NULL);
	memcpy(x, y, L2_SIZE);
    }
    gettimeofday(&tstop, NULL);
    meas =
	((tstop.tv_sec - tstart.tv_sec) * 1000000L +
	 tstop.tv_usec) - tstart.tv_usec;

    printf("L2: %.3f GByte/sec\n",
	   (double) L2_SIZE * NB_MEASURE / meas / 1000);

    free(x);
    free(y);


}
