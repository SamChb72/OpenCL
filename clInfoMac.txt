Found 1 platform(s).
platform[0x7fff0000]: profile: FULL_PROFILE
platform[0x7fff0000]: version: OpenCL 1.2 (Feb 27 2015 01:29:10)
platform[0x7fff0000]: name: Apple
platform[0x7fff0000]: vendor: Apple
platform[0x7fff0000]: extensions: cl_APPLE_SetMemObjectDestructor cl_APPLE_ContextLoggingFunctions cl_APPLE_clut cl_APPLE_query_kernel_names cl_APPLE_gl_sharing cl_khr_gl_event
platform[0x7fff0000]: Found 2 device(s).
	device[0xffffffff]: NAME: Intel(R) Core(TM) i5-4260U CPU @ 1.40GHz
	device[0xffffffff]: VENDOR: Intel
	device[0xffffffff]: PROFILE: FULL_PROFILE
	device[0xffffffff]: VERSION: OpenCL 1.2 
	device[0xffffffff]: EXTENSIONS: cl_APPLE_SetMemObjectDestructor cl_APPLE_ContextLoggingFunctions cl_APPLE_clut cl_APPLE_query_kernel_names cl_APPLE_gl_sharing cl_khr_gl_event cl_khr_fp64 cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_byte_addressable_store cl_khr_int64_base_atomics cl_khr_int64_extended_atomics cl_khr_3d_image_writes cl_khr_image2d_from_buffer cl_APPLE_fp64_basic_ops cl_APPLE_fixed_alpha_channel_orders cl_APPLE_biased_fixed_point_image_formats cl_APPLE_command_queue_priority
	device[0xffffffff]: DRIVER_VERSION: 1.1

	device[0xffffffff]: Type: CPU 
	device[0xffffffff]: EXECUTION_CAPABILITIES: Kernel Native 
	device[0xffffffff]: GLOBAL_MEM_CACHE_TYPE: Read-Write (2)
	device[0xffffffff]: CL_DEVICE_LOCAL_MEM_TYPE: Global (2)
	device[0xffffffff]: SINGLE_FP_CONFIG: 0xbf
	device[0xffffffff]: QUEUE_PROPERTIES: 0x2
	device[0xffffffff]: VENDOR_ID: 0xffffffff

	device[0xffffffff]: MAX_COMPUTE_UNITS: 4
	device[0xffffffff]: MAX_WORK_ITEM_DIMENSIONS: 3
	device[0xffffffff]: MAX_WORK_GROUP_SIZE: 1024
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_CHAR: 16
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_SHORT: 8
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_INT: 4
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_LONG: 2
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_FLOAT: 4
	device[0xffffffff]: PREFERRED_VECTOR_WIDTH_DOUBLE: 2
	device[0xffffffff]: MAX_CLOCK_FREQUENCY: 1400
	device[0xffffffff]: ADDRESS_BITS: 64
	device[0xffffffff]: MAX_MEM_ALLOC_SIZE: 1073741824
	device[0xffffffff]: IMAGE_SUPPORT: 1
	device[0xffffffff]: MAX_READ_IMAGE_ARGS: 128
	device[0xffffffff]: MAX_WRITE_IMAGE_ARGS: 8
	device[0xffffffff]: IMAGE2D_MAX_WIDTH: 8192
	device[0xffffffff]: IMAGE2D_MAX_HEIGHT: 8192
	device[0xffffffff]: IMAGE3D_MAX_WIDTH: 2048
	device[0xffffffff]: IMAGE3D_MAX_HEIGHT: 2048
	device[0xffffffff]: IMAGE3D_MAX_DEPTH: 2048
	device[0xffffffff]: MAX_SAMPLERS: 16
	device[0xffffffff]: MAX_PARAMETER_SIZE: 4096
	device[0xffffffff]: MEM_BASE_ADDR_ALIGN: 1024
	device[0xffffffff]: MIN_DATA_TYPE_ALIGN_SIZE: 128
	device[0xffffffff]: GLOBAL_MEM_CACHELINE_SIZE: 3145728
	device[0xffffffff]: GLOBAL_MEM_CACHE_SIZE: 64
	device[0xffffffff]: GLOBAL_MEM_SIZE: 4294967296
	device[0xffffffff]: MAX_CONSTANT_BUFFER_SIZE: 65536
	device[0xffffffff]: MAX_CONSTANT_ARGS: 8
	device[0xffffffff]: LOCAL_MEM_SIZE: 32768
	device[0xffffffff]: ERROR_CORRECTION_SUPPORT: 0
	device[0xffffffff]: PROFILING_TIMER_RESOLUTION: 1
	device[0xffffffff]: ENDIAN_LITTLE: 1
	device[0xffffffff]: AVAILABLE: 1
	device[0xffffffff]: COMPILER_AVAILABLE: 1
---------------------------------------------------
	device[0x1024500]: NAME: HD Graphics 5000
	device[0x1024500]: VENDOR: Intel
	device[0x1024500]: PROFILE: FULL_PROFILE
	device[0x1024500]: VERSION: OpenCL 1.2 
	device[0x1024500]: EXTENSIONS: cl_APPLE_SetMemObjectDestructor cl_APPLE_ContextLoggingFunctions cl_APPLE_clut cl_APPLE_query_kernel_names cl_APPLE_gl_sharing cl_khr_gl_event cl_khr_global_int32_base_atomics cl_khr_global_int32_extended_atomics cl_khr_local_int32_base_atomics cl_khr_local_int32_extended_atomics cl_khr_byte_addressable_store cl_khr_image2d_from_buffer cl_khr_gl_depth_images cl_khr_depth_images cl_khr_3d_image_writes 
	device[0x1024500]: DRIVER_VERSION: 1.2(Mar 27 2015 01:47:22)

	device[0x1024500]: Type: GPU 
	device[0x1024500]: EXECUTION_CAPABILITIES: Kernel 
	device[0x1024500]: GLOBAL_MEM_CACHE_TYPE: None (0)
	device[0x1024500]: CL_DEVICE_LOCAL_MEM_TYPE: Local (1)
	device[0x1024500]: SINGLE_FP_CONFIG: 0xbe
	device[0x1024500]: QUEUE_PROPERTIES: 0x2
	device[0x1024500]: VENDOR_ID: 0x1024500

	device[0x1024500]: MAX_COMPUTE_UNITS: 40
	device[0x1024500]: MAX_WORK_ITEM_DIMENSIONS: 3
	device[0x1024500]: MAX_WORK_GROUP_SIZE: 512
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_CHAR: 1
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_SHORT: 1
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_INT: 1
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_LONG: 1
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_FLOAT: 1
	device[0x1024500]: PREFERRED_VECTOR_WIDTH_DOUBLE: 0
	device[0x1024500]: MAX_CLOCK_FREQUENCY: 1000
	device[0x1024500]: ADDRESS_BITS: 64
	device[0x1024500]: MAX_MEM_ALLOC_SIZE: 402653184
	device[0x1024500]: IMAGE_SUPPORT: 1
	device[0x1024500]: MAX_READ_IMAGE_ARGS: 128
	device[0x1024500]: MAX_WRITE_IMAGE_ARGS: 8
	device[0x1024500]: IMAGE2D_MAX_WIDTH: 16384
	device[0x1024500]: IMAGE2D_MAX_HEIGHT: 16384
	device[0x1024500]: IMAGE3D_MAX_WIDTH: 2048
	device[0x1024500]: IMAGE3D_MAX_HEIGHT: 2048
	device[0x1024500]: IMAGE3D_MAX_DEPTH: 2048
	device[0x1024500]: MAX_SAMPLERS: 16
	device[0x1024500]: MAX_PARAMETER_SIZE: 1024
	device[0x1024500]: MEM_BASE_ADDR_ALIGN: 1024
	device[0x1024500]: MIN_DATA_TYPE_ALIGN_SIZE: 128
	device[0x1024500]: GLOBAL_MEM_CACHELINE_SIZE: 0
	device[0x1024500]: GLOBAL_MEM_CACHE_SIZE: 0
	device[0x1024500]: GLOBAL_MEM_SIZE: 1610612736
	device[0x1024500]: MAX_CONSTANT_BUFFER_SIZE: 65536
	device[0x1024500]: MAX_CONSTANT_ARGS: 8
	device[0x1024500]: LOCAL_MEM_SIZE: 65536
	device[0x1024500]: ERROR_CORRECTION_SUPPORT: 0
	device[0x1024500]: PROFILING_TIMER_RESOLUTION: 80
	device[0x1024500]: ENDIAN_LITTLE: 1
	device[0x1024500]: AVAILABLE: 1
	device[0x1024500]: COMPILER_AVAILABLE: 1
---------------------------------------------------
