__kernel __attribute__ ((reqd_work_group_size(1536, 1, 1)))
void copy1536(__global float* in, __global float* out)
{
  int i = get_global_id(0);
  out[i] = in[i];
  return;
}
