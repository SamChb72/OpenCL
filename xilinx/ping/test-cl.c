#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <sys/time.h>

////////////////////////////////////////////////////////////////////////////////

// Use a static matrix for simplicity
//
//#define DATA_SIZE 1024

////////////////////////////////////////////////////////////////////////////////

int load_file_to_memory(const char *filename, char **result)
{
    size_t size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
	*result = NULL;
	return -1;		// -1 means file opening fail 
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *) malloc(size + 1);
    if (size != fread(*result, sizeof(char), size, f)) {
	free(*result);
	return -2;		// -2 means file reading fail 
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}

int main(int argc, char **argv)
{
    int err;			// error code returned from api calls

    int DATA_SIZE = 2048;
    float *in;                  // original data set given to device
    float *out;                 // results returned from device
    float *sw_out;      	// results returned from device
    unsigned int correct;	// number of correct results returned

    size_t global[1];		// global domain size for our calculation
    size_t local[1];		// local domain size for our calculation

    cl_platform_id platform_id;	// platform id
    cl_device_id device_id;	// compute device id 
    cl_context context;		// compute context
    cl_command_queue commands;	// compute command queue
    cl_program program;		// compute program
    cl_kernel kernel;		// compute kernel

    char cl_platform_vendor[1001];
    char cl_platform_name[1001];

    cl_mem input;		// device memory used for the input array
    cl_mem output;		// device memory used for the output array

    int verbose, run, warmup, nb_loop;
    struct timeval utstart, utstop;
    double uduration;

    warmup = -100;
    nb_loop = 1000;

    if (6 > argc) {
	   printf("Usage & exemples:\n");
	   printf("%s $verbosityCallLevel $N $nbWarmup $nbLoop kernel\n", argv[0]);
	   printf("./%s [0..2] 1024 -100 1000 copy1.xclbin\n", argv[0]);
	   exit(0);
    } else {
	    verbose = atoi(argv[1]);
	    DATA_SIZE = atoi(argv[2]);
	    warmup = atoi(argv[3]);
	    nb_loop = atoi(argv[4]);
    }

    // Fill our data sets with pattern
    //
    int i = 0;
    in = (float *) calloc(DATA_SIZE, sizeof(float));
    out = (float *) calloc(DATA_SIZE, sizeof(float));
    sw_out = (float *) calloc(DATA_SIZE, sizeof(float));
    for (i = 0; i < DATA_SIZE; i++) {
	in[i] = (int) i;
	out[i] = 0;
    }

    // Connect to first platform
    //
    err = clGetPlatformIDs(1, &platform_id, NULL);
    if (err != CL_SUCCESS) {
	printf("Error: Failed to find an OpenCL platform!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    err =
	clGetPlatformInfo(platform_id, CL_PLATFORM_VENDOR, 1000,
			  (void *) cl_platform_vendor, NULL);
    if (err != CL_SUCCESS) {
	printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    printf("CL_PLATFORM_VENDOR %s\n", cl_platform_vendor);
    err =
	clGetPlatformInfo(platform_id, CL_PLATFORM_NAME, 1000,
			  (void *) cl_platform_name, NULL);
    if (err != CL_SUCCESS) {
	printf("Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    printf("CL_PLATFORM_NAME %s\n", cl_platform_name);

    // Connect to a compute device
    //
    int fpga = 0;
#if defined (FPGA_DEVICE)
    fpga = 1;
#endif
    err =
	clGetDeviceIDs(platform_id,
		       fpga ? CL_DEVICE_TYPE_ACCELERATOR :
		       CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
	printf("Error: Failed to create a device group!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Create a compute context 
    //
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!context) {
	printf("Error: Failed to create a compute context!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Create a command commands
    //
    commands = clCreateCommandQueue(context, device_id, 0, &err);
    if (!commands) {
	printf("Error: Failed to create a command commands!\n");
	printf("Error: code %i\n", err);
	printf("Test failed\n");
	return EXIT_FAILURE;
    }

    int status;

    // Create Program Objects
    //

    // Load binary from disk
    unsigned char *kernelbinary;
    char *xclbin = argv[5];
    printf("loading %s\n", xclbin);
    int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
    if (n_i < 0) {
	printf("failed to load kernel from xclbin: %s\n", xclbin);
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    size_t n = n_i;
    // Create the compute program from offline
    program = clCreateProgramWithBinary(context, 1, &device_id, &n,
					(const unsigned char **)
					&kernelbinary, &status, &err);
    if ((!program) || (err != CL_SUCCESS)) {
	printf("Error: Failed to create compute program from binary %d!\n",
	       err);
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Build the program executable
    //
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
	size_t len;
	char buffer[2048];

	printf("Error: Failed to build program executable!\n");
	clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
			      sizeof(buffer), buffer, &len);
	printf("%s\n", buffer);
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Create the compute kernel in the program we wish to run
    //
    char src[32], dest[32];
    bzero(src, sizeof(src));bzero(dest, sizeof(dest));
    sprintf(src, "%d", DATA_SIZE);
    strcpy(dest, "copy");
    strcat(dest, src);
	printf("%s\n", dest);
    kernel = clCreateKernel(program, dest, &err);
    if (!kernel || err != CL_SUCCESS) {
	printf("Error: Failed to create compute kernel!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Create the input and output arrays in device memory for our calculation
    //
    input =
	clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * DATA_SIZE,
		       NULL, NULL);
    output =
	clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * DATA_SIZE,
		       NULL, NULL);
    if (!input || !output) {
	printf("Error: Failed to allocate device memory!\n");
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
    // Write our data set into the input array in device memory 
    //
    //

    for (run = warmup; run < nb_loop; run++) {
	if (0 == run) {
	    gettimeofday(&utstart, NULL);
	}

	err =
	    clEnqueueWriteBuffer(commands, input, CL_TRUE, 0,
				 sizeof(float) * DATA_SIZE, in, 0, NULL,
				 NULL);
	if (err != CL_SUCCESS) {
	    printf("Error: Failed to write to source array a!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	}
	// Set the arguments to our compute kernel
	//
	err = 0;
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
	if (err != CL_SUCCESS) {
	    printf("Error: Failed to set kernel arguments! %d\n", err);
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	}
	// Execute the kernel over the entire range of our 1d input data set
	// using the maximum number of work group items for this device
	//

#ifdef C_KERNEL
	err = clEnqueueTask(commands, kernel, 0, NULL, NULL);
#else
	global[0] = DATA_SIZE;
	local[0] = DATA_SIZE;
	err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL,
				     (size_t *) & global,
				     (size_t *) & local, 0, NULL, NULL);
#endif
	if (err) {
	    printf("Error: Failed to execute kernel! %d\n", err);
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	}
	// Read back the results from the device to verify the output
	//
	cl_event readevent;
	err =
	    clEnqueueReadBuffer(commands, output, CL_TRUE, 0,
				sizeof(float) * DATA_SIZE, out, 0, NULL,
				&readevent);
	if (err != CL_SUCCESS) {
	    printf("Error: Failed to read output array! %d\n", err);
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	}
	clWaitForEvents(1, &readevent);
    }
    gettimeofday(&utstop, NULL);
    uduration =
	((utstop.tv_sec - utstart.tv_sec) * 1000000L + utstop.tv_usec) -
	utstart.tv_usec;

    printf("in\n");
    for (i = 0; i < DATA_SIZE; i++) {
	printf("%.3f ", in[i]);
    }
    printf("\n");
    printf("B\n");
    printf("out\n");
    for (i = 0; i < DATA_SIZE; i++) {
	printf("%.3f ", out[i]);
    }
    printf("\n");

    // Validate our results
    //
    correct=0;
    for (i = 0; i < DATA_SIZE; i++)
	if (out[i] == i)
	    correct++;

    // Print a brief summary detailing the results
    //
    printf("Computed '%d/%d' correct values!\n", correct, DATA_SIZE);
    printf("%d, %f\n", DATA_SIZE, uduration / nb_loop);
    // Shutdown and cleanup
    //
    clReleaseMemObject(input);
    clReleaseMemObject(output);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

    if (correct == DATA_SIZE) {
	printf("Test passed!\n");
	return EXIT_SUCCESS;
    } else {
	printf("Test failed\n");
	return EXIT_FAILURE;
    }
}
