# Define the solution for SDAccel
create_solution -name example_alpha -dir . -force
add_device -vbnv xilinx:adm-pcie-7v3:1ddr:2.0

# Host Compiler Flags
set_property -name host_cflags -value "-O3 -Wall -D FPGA_DEVICE"  -objects [current_solution]

# Host Source Files
add_files "test-cl.c"

# Kernel Definition
create_kernel copy -type clc
add_files -kernel [get_kernels copy] "copy.cl"
create_kernel copy128 -type clc
add_files -kernel [get_kernels copy128] "copy128.cl"
create_kernel copy256 -type clc
add_files -kernel [get_kernels copy256] "copy256.cl"
create_kernel copy512 -type clc
add_files -kernel [get_kernels copy512] "copy512.cl"
create_kernel copy1024 -type clc
add_files -kernel [get_kernels copy1024] "copy1024.cl"
create_kernel copy1536 -type clc
add_files -kernel [get_kernels copy1536] "copy1536.cl"
create_kernel copy2048 -type clc
add_files -kernel [get_kernels copy2048] "copy2048.cl"

# Define Binary Containers
create_opencl_binary copy1
set_property region "OCL_REGION_0" [get_opencl_binary copy1]
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy] -name k1
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy128] -name k2
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy256] -name k3
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy512] -name k4
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy1024] -name k5
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy1536] -name k6
create_compute_unit -opencl_binary [get_opencl_binary copy1] -kernel [get_kernels copy2048] -name k7


# Compile the design for CPU based emulation
compile_emulation -flow cpu -opencl_binary [get_opencl_binary copy1]

# Run the compiled application in CPU based emulation mode
run_emulation -flow cpu -args "copy1.xclbin"

set_property num_jobs 4 [current_solution] 

# Compile the application to run on the accelerator card
build_system

# Package the application binaries
package_system

