__kernel __attribute__ ((reqd_work_group_size(512, 1, 1)))
void copy512(__global float* in, __global float* out)
{
  int i = get_global_id(0);
  out[i] = in[i];
  return;
}
