__kernel __attribute__ ((reqd_work_group_size(256, 1, 1)))
void copy256(__global float* in, __global float* out)
{
  int i = get_global_id(0);
  out[i] = in[i];
  return;
}
