//__kernel  __attribute__ ((reqd_work_group_size(1024, 1, 1)))
__kernel void copy(__global float* in, __global float* out)
{
  int i = get_global_id(0);
  out[i] = in[i];
  return;
}
