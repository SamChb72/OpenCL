/***************************************************
   Copyright 2015-2025 GreenFLOPS

This software is a computer program whose purpose is
to measure and bench Heterogeneous Hardwares.

  Contact : contact@greenflops.com
****************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>
#include <CL/cl.h>

#define MAX_SOURCE_SIZE (0x100000)
#define NB_MEASURE 1

enum TEST { FFT_2 };

// bit reversing
/*
0 000 -> 000 0
1 001 -> 100 4
2 010 -> 010 2
3 011 -> 110 6
4 100 -> 001 1
5 101 -> 101 5
6 110 -> 011 3
7 111 -> 111 7
*/
unsigned char brev(int stages, unsigned char in)
{
    unsigned char out = 0;
    int j = 0;

    for (j = 0; j < stages; j++) {
	out += ((in & (1 << j)) >> j) << (stages - 1 - j);
    }
    return out;
}


int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int meas2[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 1;
    unsigned char stage = 3;
    int verbose = 0;
    int i = 0;
    int j = 0;
    int c;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;
    enum TEST test = FFT_2;	// 0=-copy; 1=-flops 2=-fft2

    // Host input vectors
    float *x;
    float *y;
    float *X;
    float *Y;
    float *tmp;

    // device type CPU or GPU (Default)
    int device_type = CL_DEVICE_TYPE_CPU;


    // Device input buffers
    cl_mem d_x;
    cl_mem d_y;
    cl_mem d_X;
    cl_mem d_Y;

    cl_platform_id cpPlatform;	// OpenCL platform
    cl_device_id device_id;	// device ID
    cl_context context;		// context
    cl_command_queue queue;	// command queue
    cl_program program;		// program
    cl_kernel kernel;		// kernels

    if (2 > argc) {
	printf("Usage & exemples:\n");

    } else {
	stage = atoi(argv[pos]);
	size = pow(2, stage);
	printf("%d stages => N=%d\n", stage, size);
    }
    size *= sizeof(float);
    // Allocate memory for each vector on host
    x = (float *) malloc(size);
    X = (float *) malloc(size);
    y = (float *) malloc(size);
    Y = (float *) malloc(size);
    tmp = (float *) malloc(size);
    // Initialize vectors on host
    for (i = 0; i < size / sizeof(float); i++) {
	x[i] = 2*i;
    }
    for (i = 1; i < size / sizeof(float); i++) {
	y[i] = 2*i+1;
    }
    for (i = 0; i < size / sizeof(float); i += 1) {
	X[i] = 0.0f;
	Y[i] = 0.0f;
    }
/*
    x[0] = 1.0f;
    x[1] = 1.0f;
    x[2] = 0.0f;
    x[3] = 0.0f;
    x[4] = 0.0f;
    x[5] = 0.0f;
    x[6] = 0.0f;
    x[7] = 0.0f;
    x[8] = 1.0f;
    x[9] = 0.0f;
    x[10] = 1.0f;
    x[11] = 0.0f;
    x[12] = 1.0f;
    x[13] = 0.0f;
    x[14] = 1.0f;
    x[15] = 0.0f;
*/
/*
    for (i = 0; i < size / sizeof(float); i++) {
	printf("x[%d] = %.1f\n", i, x[i]);
    }
*/
    for (i = 0; i < size / sizeof(float); i++) {
	tmp[i] = x[brev(stage, i)];
    }
    for (i = 0; i < size / sizeof(float); i++) {
	x[i] = tmp[i];
    }
/*
    for (i = 0; i < size / sizeof(float); i++) {
	printf("revx[%d] = %.1f\n", i, x[i]);
    }
*/
    size_t globalSize, localSize;
    cl_int err;

    FILE *fp;
    char *source_str, *include_str;
    size_t source_size, include_size;

    /* Load the source code containing the kernel */
    fp = fopen("./radix2.cl", "r");
    if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    /* Load the include file */
    const char *strings[1];
    strings[0] = source_str;
    size_t lenghts[1];
    lenghts[0] = source_size;

    // Number of work items in each local work group
    localSize = 256;

    // Number of total work items - localSize must be devisor
    globalSize =
	ceil(size / sizeof(float) / (float) localSize) * localSize;

    // Bind to platform
    err = clGetPlatformIDs(1, &cpPlatform, NULL);

    // Get ID for the device
    err = clGetDeviceIDs(cpPlatform, device_type, 1, &device_id, NULL);

    // Create a context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    // Create a command queue
    queue =
	clCreateCommandQueueWithProperties(context, device_id, 0, &err);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1,
					(const char **) &strings,
					(const size_t *) lenghts, &err);
    if (CL_SUCCESS != err)
	printf("error = %d\n", err);
    // Build the program executable
    err |= clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (CL_SUCCESS != err)
	printf("error = %d\n", err);
    printf("Hello\n");
    // Create the compute kernel in the program we wish to run
    kernel = clCreateKernel(program, "fft", &err);
    // Create the input and output arrays in device memory for our calculation
    d_x = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, NULL);
    d_y = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, NULL);
    d_X = clCreateBuffer(context, CL_MEM_WRITE_ONLY, size, NULL, NULL);
    d_Y = clCreateBuffer(context, CL_MEM_WRITE_ONLY, size, NULL, NULL);

    if (cache) {
	start = -10;
	if (verbose)
	    printf("cache option SET\n");
    } else {
	start = 0;
	if (verbose)
	    printf("no cache option SET\n");
    }
    int run = 0;
    for (run = 0; run < NB_MEASURE; run++) {
	gettimeofday(&tstart, NULL);
	for (i = 0; i < stage; i++) {
#if 0
	    printf("Stage %d:\n", i + 1);
	    for (j = 0; j < size / sizeof(float); j++) {
		printf("x[%d] = %.3f\n", j, x[j]);
	    }
	    for (j = 0; j < size / sizeof(float); j++) {
		printf("y[%d] = %.3f\n", j, y[j]);
	    }
#endif
	    // Write our data set into the input array in device memory
	    err = clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0,
				       size, x, 0, NULL, NULL);
	    err = clEnqueueWriteBuffer(queue, d_y, CL_TRUE, 0,
				       size, y, 0, NULL, NULL);

	    // Set the arguments to our compute kernel
	    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_x);
	    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_y);
	    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_X);
	    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_Y);
	    err |= clSetKernelArg(kernel, 4, sizeof(int), &i);	//i = stage - 1

	    // Execute the kernel over the entire range of the data set 
	    err =
		clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize,
				       &localSize, 0, NULL, NULL);

	    // Wait for the command queue to get serviced before reading back results
	    clFinish(queue);

	    // Read the results from the device
	    if (i + 1 < stage) {
		clEnqueueReadBuffer(queue, d_X, CL_TRUE, 0, size, x, 0,
				    NULL, NULL);
		clEnqueueReadBuffer(queue, d_Y, CL_TRUE, 0, size, y, 0,
				    NULL, NULL);
	    } else {
		clEnqueueReadBuffer(queue, d_X, CL_TRUE, 0, size, X, 0,
				    NULL, NULL);
		clEnqueueReadBuffer(queue, d_Y, CL_TRUE, 0, size, Y, 0,
				    NULL, NULL);
	    }
#if 1
	    for (j = 0; j < size / sizeof(float); j++) {
		printf("X[%d] = %.3f\n", j, X[j]/size*4);
	    }
	    for (j = 0; j < size / sizeof(float); j++) {
		printf("Y[%d] = %.3f\n", j, Y[j]/size*4);
	    }
#endif
	}
	gettimeofday(&tstop, NULL);
	meas[run] =
	    ((tstop.tv_sec - tstart.tv_sec) * 1000000L +
	     tstop.tv_usec) - tstart.tv_usec;
    }

    for (run = 0; run < NB_MEASURE; run++) {
	mean += meas[run];
    }

    mean /= NB_MEASURE;
    for (run = 0; run < NB_MEASURE; run++) {
	var += ((meas[run] - mean) * (meas[run] - mean));
    }

    var /= NB_MEASURE;
    printf(";N\t;NBytes\t;avg(usec)\t;variance\t;FFT Speed GB/s;\n");
    printf(";%lu\t;%u\t;%lu\t;%lu\t;%.3f;\n", size / sizeof(float),
	   size, mean, var, (float) size / (float) mean);
    // release OpenCL resources
#if 0
    clReleaseMemObject(d_x);
    clReleaseMemObject(d_y);
    clReleaseMemObject(d_X);
    clReleaseMemObject(d_Y);
    
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    
    //release host memory
    
    free(x);
    free(y);
    free(X);
    free(Y);
    free(tmp);
#endif

    return 0;
}
