__kernel void
fft(__global float *x, __global float *y, __global float *X,
    __global float *Y, int s)
{
    int i = get_global_id(0);
    int n = 1 << (s + 1);	// n = 2, 4, 8, 16, 32, ...
    if ((i % n) < (n / 2)) {
	float a = -2.0 * (float) i / (float) n;
	float ReW = cospi(a);
	float ImW = sinpi(a);
	int j = i + n / 2;
	X[i] = x[i] + x[j] * ReW - y[j] * ImW;
	X[j] = x[i] - x[j] * ReW + y[j] * ImW;
	Y[i] = y[i] + y[j] * ReW + x[j] * ImW;
	Y[j] = y[i] - y[j] * ReW - x[j] * ImW;
    }
}
