/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

#define NB_MEASURE 100
#define NANOINUSEC 1000

#define DEBUG 1
#undef DEBUG

#define UINT16_SIZE 2

void print(const char *name, cl_short * v, unsigned int size)
{
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
	printf("%s[%d]=%d\n", name, i, v[i]);
    }
}

int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 4;
    unsigned char stage = 3;
    int verbose = 0;
    int i = 0;
    int j = 0;
    int c;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;

    //device type CPU or GPU(Default)
    int device_type = CL_DEVICE_TYPE_DEFAULT;

    cl_platform_id cpPlatform;
    cl_device_id device_id;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;

    if (2 > argc) {
	printf("Usage & exemples:\n");
	printf("./tests length\n");
	printf("./tests 1024 \n");
	size = 1024;
    }
    size *= UINT16_SIZE;

    //Allocate memory for each vector on host
    cl_short *x = (cl_short *) malloc(size);

    size_t global, local;
    cl_int err;

    FILE *fp;
    char *source_str, *include_str;
    size_t source_size, include_size;

    /* Load the source code containing the kernel */
    fp = fopen("./tests.cl", "r");
    if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    /* Load the include file */
    const char *strings[1];
    strings[0] = source_str;
    size_t lenghts[1];
    lenghts[0] = source_size;

    //Bind to platform
    err = clGetPlatformIDs(1, &cpPlatform, NULL);

    //Get ID for the device
    err = clGetDeviceIDs(cpPlatform, device_type, 1, &device_id, NULL);

    //Create a context
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    //Create a command queue
#ifdef CL_VERSION_2_0
    queue =
	clCreateCommandQueueWithProperties(context, device_id, 0, &err);
#else
    queue = clCreateCommandQueue(context, device_id, 0, &err);
#endif

    //Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1,
					(const char **) &strings,
					(const size_t *) lenghts, &err);
    if (CL_SUCCESS != err)
	printf("error = %d\n", err);
    //Build the program executable
    err |= clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (CL_SUCCESS != err)
	printf("error = %d\n", err);

    //Create the compute kernel in the program we wish to run
    kernel = clCreateKernel(program, "test", &err);

    //Create the input and output arrays in device memory for our calculation
    cl_mem d_x =
	clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, NULL);

    //Set global and local workgroups size
    clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE,
		    sizeof(global), &global, NULL);
    err |=
	clGetKernelWorkGroupInfo(kernel, device_id,
				 CL_KERNEL_WORK_GROUP_SIZE, sizeof(local),
				 &local, NULL);
    if (CL_SUCCESS != err)
	printf("error = %d\n", err);

    local /= 2;
    int run = 0;

    for (run = 0; run < NB_MEASURE; run++) {
	//Initialize vectors
	memset(x, 0, size);
	for (i = 0; i < size / UINT16_SIZE; i++) {
	    x[i] = 1;
	}
#if DEBUG
	print("x", x, size / UINT16_SIZE);
#endif
	int nbfft;
	gettimeofday(&tstart, NULL);
	for (nbfft = 0; nbfft < NANOINUSEC; nbfft++) {
	    //Write our data set into the input array in device memory
	    err = clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0,
				       size, x, 0, NULL, NULL);
#if DEBUG
	    print("x", x, size / UINT16_SIZE);
	    print("y", y, size / UINT16_SIZE);
#endif
	    //Set the arguments to our compute kernel
	    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_x);

	    // Execute the kernel over the entire range of the data set
	    err =
		clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
				       &global, &local, 0, NULL, NULL);

	    //Wait for the command queue to get serviced before reading back results
	    clFinish(queue);
	}
	gettimeofday(&tstop, NULL);
	meas[run] =
	    ((tstop.tv_sec - tstart.tv_sec) * 1000000L +
	     tstop.tv_usec) - tstart.tv_usec;
    }

    clEnqueueReadBuffer(queue, d_x, CL_TRUE, 0, size, x, 0, NULL, NULL);


    for (run = 0; run < NB_MEASURE; run++) {
	mean += meas[run];
    }

    mean /= NB_MEASURE;
    for (run = 0; run < NB_MEASURE; run++) {
	var += ((meas[run] - mean) * (meas[run] - mean));
    }

    var /= NB_MEASURE;

    //printf("N,bytes,avg[usec],variance,FFT Rate [FFT/s]\n");
    printf("%d,%u,%lu,%lu,%.3f\n", size / UINT16_SIZE,
	   size, mean, var, 1000000.0f / (float) mean);
    //release OpenCL resources
    clReleaseMemObject(d_x);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    //release host memory
    free(x);

    return 0;
}
