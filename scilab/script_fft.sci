//////////////////////////////////////////////////////////
//       Copyright (C) 2015 GreenFLOPS          	//
//                                                      //
// This file is part of GreenFLOPS projects and can not //
// be copied and/or distributed in any medium or format //
//    without the express permission of GreenFLOPS      //
//                                                      //
//              contact@greenflops.com                  //
//////////////////////////////////////////////////////////

N = input(" Rentrer la valeur de N : ");

// Create complex vector [(0, i), (2, 3i), (4, 5i), ... ]
v = complex([0:2:2*(N-1)], [1:2:2*(N-1)+1])

// Create complex vector [ (1, i), (1, i), (1, i)... ]
w = complex(ones(1, N), ones(1, N))

// Compute FFT
V = fft(v,-1)
W = fft(w,-1)

//Output results in a csv file
write_csv(V, './scilab_fft_V_' + string(N) + '.csv')
write_csv(W, './scilab_fft_W_' + string(N) + '.csv')

// Compute iFFT
V = fft(v,1)
W = fft(w,1)

//Output results in a csv file
write_csv(V, './scilab_ifft_V_' + string(N) + '.csv')
write_csv(W, './scilab_ifft_W_' + string(N) + '.csv')

