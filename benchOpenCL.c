/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)
#define NB_MEASURE 1000
#define WARMUP 0

enum TEST { FLOPS_0, FLOPS_1 };

int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int meas2[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 1;
    int verbose = 0;
    int i = 0;
    int c;
    int warmup = WARMUP;
    int coef = 1;
    int pos = 1;
    int start = 0;
    enum TEST test = FLOPS_0;	// 0=-copy; 1=-flops 2=-fft2

    // Host input vectors
    short *in;
    short *out;

    // device type CPU or GPU (Default)
    int device_type = CL_DEVICE_TYPE_DEFAULT;


    // Device input buffers
    cl_mem d_in;
    cl_mem d_out;

    cl_platform_id cpPlatform;	// OpenCL platform
    cl_device_id device_id;	// device ID
    cl_context context;		// context
    cl_command_queue queue;	// command queue
    cl_program program;		// program
    cl_kernel kernel;		// kernel

    if (2 > argc) {
	printf("Usage & exemples:\n");
	printf
	    ("benchOpenCL typeOfTest {-flop0, -flop1, } cacheOption {warmup nb_warmup_loops} vectorSize {xxx, xxxkB, xxxMB, xxxGB}\n");
	printf("benchOpenCL -flop0 100kb\n");
	printf("benchOpenCL -flop0 warmup 20 100kB\n");
	printf("benchOpenCL -flop1 10MB\n");
	printf("benchOpenCL -flop1 warmup 100 1GB\n");
	printf("\n");
	printf("\n");
	printf("Run default case ./benchOpenCL -flop1\n");

    } else {
	if ('-' == argv[pos][0]) {
	    if (!strcmp(argv[pos], "-flops0"))
		test = FLOPS_0;
	    else if (!strcmp(argv[pos], "-flops1"))
		test = FLOPS_1;
	    pos++;
	    if (verbose)
		printf("test=%d\n", test);
	}
	if (!strcmp("warmup", argv[pos])) {
	    pos++;
	    warmup = atoi(argv[pos++]);
	}
	if (0 != strcasestr(argv[pos], "kB"))
	    coef = 1024;
	else if (0 != strcasestr(argv[pos], "MB"))
	    coef = 1024 * 1024;
	else if (0 != strcasestr(argv[pos], "GB"))
	    coef = 1024 * 1024 * 1024;
	size *= coef * atoi(argv[pos]);
    }
    if (1 == coef) {
	size *= sizeof(short);
    }
    // Allocate memory for each vector on host
    in = (short *) malloc(size);
    out = (short *) malloc(size);
    // Initialize vectors on host
    for (i = 0; i < size / sizeof(short); i++) {
	in[i] = (short) i;
	out[i] = 0.0f;
    }

    size_t globalSize, localSize;
    cl_int err;

    FILE *fp;
    char *source_str;
    size_t source_size;

    /* Load the source code containing the kernel */
    fp = fopen("./kernels.cl", "r");
    if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
    }
    source_str = (char *) malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    // Number of work items in each local work group
    localSize = 256;

    // Number of total work items - localSize must be devisor
    globalSize =
	ceil(size / sizeof(short) / (short) localSize) * localSize;

    // Bind to platform
    err = clGetPlatformIDs(1, &cpPlatform, NULL);

    // Get ID for the device
    err = clGetDeviceIDs(cpPlatform, device_type, 1, &device_id, NULL);

    // Create a context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    // Create a command queue
#ifdef CL_VERSION_2_0
    queue =
	clCreateCommandQueueWithProperties(context, device_id, 0, &err);
#else
    queue = clCreateCommandQueue(context, device_id, 0, &err);
#endif

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1,
					(const char **) &source_str, NULL,
					&err);

    // Build the program executable
    clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    // Create the compute kernel in the program we wish to run
    if (FLOPS_0 == test) {
	kernel = clCreateKernel(program, "flop_0", &err);
    } else if (FLOPS_1 == test) {
	kernel = clCreateKernel(program, "flop_1", &err);
    }
    // Create the input and output arrays in device memory for our calculation
    d_in = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, NULL);
    d_out = clCreateBuffer(context, CL_MEM_WRITE_ONLY, size, NULL, NULL);

    for (i = -warmup; i < NB_MEASURE; i++) {
	if (0 <= i)		// Start measure after warmup
	    gettimeofday(&tstart, NULL);
	// Write our data set into the input array in device memory
	err = clEnqueueWriteBuffer(queue, d_in, CL_TRUE, 0,
				   size, in, 0, NULL, NULL);

	// Set the arguments to our compute kernel
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_in);
	err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_out);

	// Execute the kernel over the entire range of the data set 
	err =
	    clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize,
				   &localSize, 0, NULL, NULL);

	// Wait for the command queue to get serviced before reading back results
	clFinish(queue);

	// Read the results from the device
	clEnqueueReadBuffer(queue, d_out, CL_TRUE, 0, size, out, 0,
			    NULL, NULL);
	gettimeofday(&tstop, NULL);
	if (0 <= i)
	    meas[i] =
		((tstop.tv_sec - tstart.tv_sec) * 1000000L +
		 tstop.tv_usec) - tstart.tv_usec;
    }
    for (i = 0; i < NB_MEASURE; i++) {
	mean += meas[i];
    }
    mean /= NB_MEASURE;
    for (i = 0; i < NB_MEASURE; i++) {
	var += ((meas[i] - mean) * (meas[i] - mean));
    }
    var /= NB_MEASURE;
    if (FLOPS_0 == test) {
	printf("%lu,%u,%lu,%lu,%.3f\n", size / sizeof(short),
	       size, mean, var, 1000000.0f / (short) mean);
#if 0
	printf
	    ("OpenCL: Vector of %lu shorts of %d-bytes = %u Bytes takes %lu usec [+/-var %lu] => COPY Speed = %.3f GByte/s\n",
	     (size / sizeof(short)), (int) sizeof(short), size, mean,
	     var, (short) size / (short) mean);
#endif
    } else if (FLOPS_1 == test) {
	printf("%lu,%u,%lu,%lu,%.3f\n",
	       size / sizeof(short), size, mean, var,
	       (short) (1.0f * size / sizeof(short) / mean));
#if 0
	printf
	    ("OpenCL: Vector of %lu shorts of %d-bytes = %u Bytes takes %lu usec [+/-var %lu] => FLOPS Speed = %.3f Gflops\n",
	     (size / sizeof(short)), (int) sizeof(short), size, mean,
	     var, (short) (1.0f * size / sizeof(short) / mean));
#endif
    }
    //Validate
/*
    int valid = 1;
    int failIndex = 0;
    if (FLOPS_0 == test) {
	for (i = 0; i < (size / sizeof(short)); i++) {
	    if (verbose && i < 10)
		printf("in[%d]=%f, out[%d]=%f, ", i, in[i], i, out[i]);
	    if (out[i] != in[i]) {
		failIndex = i;
		valid = 0;
		break;
	    }
	}
	if (valid) {
	    if (verbose)
		printf("passed validation\n");
	} else
	    printf("VALIDATION FAILED!\nBad index: %d\n", failIndex);
    } else if (FLOPS_1 == test) {
	for (i = 0; i < (size / sizeof(short)); i++) {
	    //printf("out[%d]=%.1f", i, out[i]);
	    if (out[i] != 555.0f - (short) i) {
		failIndex = i;
		valid = 0;
		break;
	    }
	}
	if (valid) {
	    if (verbose)
		printf("passed validation\n");
	} else
	    printf("VALIDATION FAILED!\nBad index: %d\n", failIndex);
    }
*/
    // release OpenCL resources
    clReleaseMemObject(d_in);
    clReleaseMemObject(d_out);

    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    //release host memory
    free(in);
    free(out);

    return 0;
}
