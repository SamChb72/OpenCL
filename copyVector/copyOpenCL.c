/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)
#define EXIT_FAILURE 1

#ifdef __APPLE__
#define CLOCK_PROCESS_CPUTIME_ID 1
//clock_gettime is not implemented on OSX
int
clock_gettime (int clk_id, struct timespec *t)
{
  struct timeval now;
  int rv = gettimeofday (&now, NULL);

  if (rv)
    return rv;
  t->tv_sec = now.tv_sec;
  t->tv_nsec = now.tv_usec * 1000;
  return 0;
}
#endif

int
main (int argc, char *argv[])
{
  //Set the size
  int N = 1024;
  int nb_loop = 1000;
  int verbose = 0;
  int warmup = 0;
  int testcase = 1;
  int DATA_SIZE = 2 * N;
  int globalSize = 256;
  int i = 0;
  char *platformName;
  char *deviceType;
  int device;
  int verif;

  /* OpenCL variables */
  size_t global[1];		// global domain size for our calculation
  size_t local[1];		// local domain size for our calculation
  int err;			// error code returned from api calls

  cl_platform_id platform_id[2];	// platform id
  cl_device_id device_id;	// compute device id 
  cl_context context;		// compute context
  cl_command_queue commands;	// compute command queue
  cl_program program;		// compute program
  cl_kernel kernel;		// compute kernel

  char cl_platform_vendor[1001];
  char cl_platform_name[1001];

  cl_mem input;			// device memory used for the input array
  cl_mem output;		// device memory used for the output array


  if (9 > argc)
    {
      printf ("Usage & exemples:\n");
      printf
	("%s $testcase $verbosity $N $nbWarmup $nbLoop $globalSize $platformName $deviceType\n",
	 argv[0]);
      printf ("%s 1 [0..2] 1024 -100 1000 256 AMD GPU\n", argv[0]);
      exit (0);
    }
  else
    {
      testcase = atoi (argv[1]);
      verbose = atoi (argv[2]);
      N = atoi (argv[3]);
      warmup = atoi (argv[4]);
      nb_loop = atoi (argv[5]);
      DATA_SIZE = 2 * N;
      globalSize = atoi (argv[6]);
      platformName = argv[7];
      deviceType = argv[8];
      if (NULL != strcasestr (deviceType, "CPU"))
	device = CL_DEVICE_TYPE_CPU;
      else if (NULL != strcasestr (deviceType, "GPU"))
	device = CL_DEVICE_TYPE_GPU;
    }

  float *Src = (float *) malloc (2 * N * sizeof (*Src));
  float *Src2 = (float *) malloc (2 * N * sizeof (*Src2));
  int print_iter = 0;
  while (print_iter < 2 * N)
    {
      float x = print_iter;
      float y = x + 1;
      Src[print_iter] = x;
      Src[print_iter + 1] = y;
      //printf("(%f, %f) ", x, y);
      if (1 < verbose)
	printf ("%f+%fi,", Src[print_iter], Src[print_iter + 1]);

      // SRC2
      Src2[print_iter] = Src2[print_iter + 1] = 1;
      if (1 < verbose)
	printf ("%f+%fi,", Src2[print_iter], Src2[print_iter + 1]);
      print_iter += 2;
    }


  if (1 < verbose)
    printf ("\n");


  float *Dst = (float *) malloc (2 * N * sizeof (*Dst));

  if (2 == testcase)
    {
      /* OpenCL testcase */
      FILE *fp;
      char *source_str, *include_str;
      size_t source_size, include_size;

      /* Load the source code containing the kernel */
      fp = fopen ("copy.cl", "r");
      if (!fp)
	{
	  fprintf (stderr, "Failed to load kernel.\n");
	  exit (1);
	}
      source_str = (char *) malloc (10 * 1024);
      source_size = fread (source_str, 1, 10 * 1024, fp);
      fclose (fp);

      /* Load the include file */
      const char *strings[1];
      strings[0] = source_str;
      size_t lenghts[1];
      lenghts[0] = source_size;
      cl_uint num_platforms;

      err = clGetPlatformIDs (2, platform_id, &num_platforms);
      if (err != CL_SUCCESS)
	{
	  printf ("Error: Failed to find an OpenCL platform!\n");
	  printf ("Test failed\n");
	  return EXIT_FAILURE;
	}
      for (i = 0; i < num_platforms; i++)
	{
	  err =
	    clGetPlatformInfo (platform_id[i], CL_PLATFORM_VENDOR, 1000,
			       (void *) cl_platform_vendor, NULL);
	  if (err != CL_SUCCESS)
	    {
	      printf
		("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
	      printf ("Test failed\n");
	      return EXIT_FAILURE;
	    }
	  if (1 <= verbose)
	    printf ("CL_PLATFORM_VENDOR %s\n", cl_platform_vendor);
	  err =
	    clGetPlatformInfo (platform_id[i], CL_PLATFORM_NAME, 1000,
			       (void *) cl_platform_name, NULL);
	  if (err != CL_SUCCESS)
	    {
	      printf ("Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!\n");
	      printf ("Test failed\n");
	      return EXIT_FAILURE;
	    }
	  if (1 <= verbose)
	    printf ("CL_PLATFORM_NAME %s\n", cl_platform_name);

	  err = clGetDeviceIDs (platform_id[i], device, 1, &device_id, NULL);
	  if (err != CL_SUCCESS)
	    {
	      printf ("Error: Failed to create a device group!\n");
	      printf ("Test failed\n");
	      return EXIT_FAILURE;
	    }
	  if (NULL != strcasestr (cl_platform_name, platformName))
	    break;
	}
      // Create a compute context 
      //
      context = clCreateContext (0, 1, &device_id, NULL, NULL, &err);
      if (!context)
	{
	  printf ("Error: Failed to create a compute context!\n");
	  printf ("Test failed\n");
	  return EXIT_FAILURE;
	}
      // Create a command commands
      //
      commands = clCreateCommandQueue (context, device_id, 0, &err);
      if (!commands)
	{
	  printf ("Error: Failed to create a command commands!\n");
	  printf ("Error: code %i\n", err);
	  printf ("Test failed\n");
	  return EXIT_FAILURE;
	}

      int status;
      //Create the compute program from the source buffer
      program = clCreateProgramWithSource (context, 1,
					   (const char **) &strings,
					   (const size_t *) lenghts, &err);
      if (CL_SUCCESS != err)
	printf ("error = %d\n", err);
      char options[] = "-cl-mad-enable";
      //Build the program executable
      err |= clBuildProgram (program, 0, NULL, options, NULL, NULL);

      if (CL_SUCCESS != err)
	printf ("error = %d\n", err);

      //Create the compute kernel in the program we wish to run
      kernel = clCreateKernel (program, "copy", &err);

      // Create the input and output arrays in device memory for our calculation
      //
      input =
	clCreateBuffer (context, CL_MEM_READ_ONLY,
			sizeof (float) * DATA_SIZE, NULL, NULL);
      output =
	clCreateBuffer (context, CL_MEM_WRITE_ONLY,
			sizeof (float) * DATA_SIZE, NULL, NULL);
      if (!input || !output)
	{
	  printf ("Error: Failed to allocate device memory!\n");
	  printf ("Test failed\n");
	  return EXIT_FAILURE;
	}
    }

  {
    int run = 0;
    struct timeval utstart, utstop;
    double uduration;

    for (run = warmup; run < nb_loop; run++)
      {
	if (0 == run)
	  {
	    gettimeofday (&utstart, NULL);
	  }

	if (1 == testcase)
	  {
	    if (!(run % 2))
	      memcpy (Dst, Src, 2 * N * sizeof (float));
	    else
	      memcpy (Dst, Src2, 2 * N * sizeof (float));
	  }
	else if (2 == testcase)
	  {
	    if (!(run % 2))
	      {
		err =
		  clEnqueueWriteBuffer (commands, input, CL_TRUE, 0,
					sizeof (float) * DATA_SIZE,
					Src, 0, NULL, NULL);
	      }
	    else
	      {
		err =
		  clEnqueueWriteBuffer (commands, input, CL_TRUE, 0,
					sizeof (float) * DATA_SIZE,
					Src2, 0, NULL, NULL);
	      }
	    if (err != CL_SUCCESS)
	      {
		printf ("Error: Failed to write to source array a!\n");
		printf ("Test failed\n");
		return EXIT_FAILURE;
	      }
	    // Set the arguments to our compute kernel
	    //
	    err = 0;
	    err = clSetKernelArg (kernel, 0, sizeof (cl_mem), &input);
	    err |= clSetKernelArg (kernel, 1, sizeof (cl_mem), &output);
	    if (err != CL_SUCCESS)
	      {
		printf ("Error: Failed to set kernel arguments! %d\n", err);
		printf ("Test failed\n");
		return EXIT_FAILURE;
	      }
	    // Execute the kernel over the entire range of our 1d input data set
	    // using the maximum number of work group items for this device
	    //

#ifdef C_KERNEL
	    err = clEnqueueTask (commands, kernel, 0, NULL, NULL);
#else
	    global[0] = DATA_SIZE;//globalSize;
	    //local[0] = DATA_SIZE / globalSize;
	    err = clEnqueueNDRangeKernel (commands, kernel, 1, NULL,
					  (size_t *) & global,
					  NULL, 0, NULL, NULL);
#endif
	    if (err)
	      {
		printf ("Error: Failed to execute kernel! %d\n", err);
		printf ("Test failed\n");
		return EXIT_FAILURE;
	      }
	    // Read back the results from the device to verify the output
	    //
	    cl_event readevent;
	    err =
	      clEnqueueReadBuffer (commands, output, CL_TRUE, 0,
				   sizeof (float) * DATA_SIZE, Dst, 0,
				   NULL, &readevent);
	    if (err != CL_SUCCESS)
	      {
		printf ("Error: Failed to read output array! %d\n", err);
		printf ("Test failed\n");
		return EXIT_FAILURE;
	      }
	    clWaitForEvents (1, &readevent);

	  }
      }
    gettimeofday (&utstop, NULL);
    uduration =
      ((utstop.tv_sec - utstart.tv_sec) * 1000000L +
       utstop.tv_usec) - utstart.tv_usec;


    print_iter = 0;
    verif = 1;
    while (print_iter < 2 * N)
      {
	if (nb_loop % 2)
	  {
	    if ((Dst[print_iter] != Src[print_iter])
		|| (Dst[print_iter + 1] != Src[print_iter + 1]))
	      verif = 0;
	  }
	else
	  {
	    if ((Dst[print_iter] != Src2[print_iter])
		|| (Dst[print_iter + 1] != Src2[print_iter + 1]))
	      verif = 0;
	  }
	print_iter += 2;
      }
    if (1 <= verbose)
      {
	if (!verif)
	  printf ("validation failed\n");
	else
	  printf ("validation OK\n");
      }
    if (1 < verbose)
      {
	print_iter = 0;
	while (print_iter < 2 * N)
	  {
	    printf ("%f+%fi,", Dst[print_iter], Dst[print_iter + 1]);
	    print_iter += 2;
	  }
	printf ("\n");
      }

    printf ("%f\n", uduration / nb_loop);
  }
}
