/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <libelf.h>
#include <iostream>
#include <sys/time.h>
#include <getopt.h>
#include "fft.h"
#include <math.h>

#define NB_MEASURE 1
#define NANOINUSEC 1

#define DEBUG 1
#undef DEBUG
#define UINT16_SIZE 2

unsigned int num;


void print(const char *name, int16_t * v, unsigned int size)
{
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
	printf("%s[%d]=%d\n", name, i, v[i]);
    }
}


// bit reversing
/*
0 000 -> 000 0
1 001 -> 100 4
2 010 -> 010 2
3 011 -> 110 6
4 100 -> 001 1
5 101 -> 101 5
6 110 -> 011 3
7 111 -> 111 7
*/
unsigned char brev(int stages, unsigned char in)
{
    unsigned char out = 0;
    int j = 0;

    for (j = 0; j < stages; j++) {
	out += ((in & (1 << j)) >> j) << (stages - 1 - j);
    }
    return out;
}

int main(int argc, char **argv)
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int mean = 0;
    unsigned long int var = 0;
    unsigned int size = 4;
    unsigned char stage = 5;
    int verbose = 0;
    int i = 0;
    int c, j;
    int cache = 1;
    int coef = 1;
    int pos = 1;
    int start = 0;
    int radix = 4;
    int zoom = 2;
    int length;
    int total_length;
    int number;

    if (4 > argc) {
	printf("Usage & exemples:\n");
	printf("./fft radix stages zoom\n");
	printf
	    ("./fft 4 5 2 to compute the fft radix 4 algo in 5 stages (1024 elem) for 4x4 = 16 times\n");
	radix = 4;
	stage = 5;
	zoom = 2;
	length = pow(4, stage);
    } else {
	radix = atoi(argv[pos++]);
	stage = atoi(argv[pos++]);
	zoom = atoi(argv[pos++]);
	if (4 == radix)
	    length = pow(4, stage);
	else if (2 == radix)
	    length = pow(2, stage);
    }
    total_length = length;
    for (i = 0; i < zoom; i++) {
	total_length *= radix;
    }
    number = total_length / length;
    size = total_length * UINT16_SIZE;

    //Setup kernel arguments
    int16_t *x = (int16_t *) malloc(size);
    int16_t *y = (int16_t *) malloc(size);
    int16_t *X = (int16_t *) malloc(size);
    int16_t *Y = (int16_t *) malloc(size);
    int16_t *tmp = (int16_t *) malloc(size);

    SNK_INIT_LPARM(lparm, total_length);
//	lparm->ldims[0] = 512;
    //int streams = -1;
    //lparm->stream = streams;
    //lparm->barrier = SNK_UNORDERED;
    int run, s;
    memset(X, 0, size);
    memset(Y, 0, size);
    for (run = -100; run < NB_MEASURE; run++) {
	// Initialize vectors on host
	memset(x, 0, size);
	memset(y, 0, size);
	memset(tmp, 0, size);
	for (i = 0; i < total_length; i++) {
	    x[i] = 1.0f;
	}
	//revert
	for (j = 0; j < number; j++) {
	    for (i = 0; i < length; i++) {
		tmp[i + j * length] = x[brev(stage, i) + j * length];
	    }
	}

	int t = 0;
	if (run >= 0)
	    gettimeofday(&tstart, NULL);
	for (t = 0; t < NANOINUSEC; t++) {
	    memcpy(x, tmp, size);
	    for (s = 0; s < stage; s++) {
#ifdef DEBUG
		printf("Stage %d:\n", s + 1);
		print("x", x, 10);
		print("y", y, 10);
#endif
		if (4 == radix)
		    dit4(x, y, s, size / UINT16_SIZE, lparm);
		else if (2 == radix)
		    dit2(x, y, s, size / UINT16_SIZE, lparm);
		//stream_sync(streams);
	    }
	}
	gettimeofday(&tstop, NULL);
	if (run >= 0) {
	    meas[run] =
		((tstop.tv_sec - tstart.tv_sec) * 1000000L +
		 tstop.tv_usec) - tstart.tv_usec;
	}
    }

    memcpy(X, x, size);
    memcpy(Y, y, size);
    for (run = 0; run < NB_MEASURE; run++) {
	mean += meas[run];
    }

    mean /= NB_MEASURE;
    for (run = 0; run < NB_MEASURE; run++) {
	var += ((meas[run] - mean) * (meas[run] - mean));
    }

    var /= NB_MEASURE;
//#ifdef DEBUG
    print("X", X, 5);
//    print("Y", Y, 5);
//#endif
    int valid = 1;
    for (j = 0; j < number; j++) {
        if (length != X[j * length])
            valid = 0;
        for (i = 1 + j * length; i < (j + 1) * length; i++) {
            if (0 != X[i]) {
                valid = 0;
                break;
            }
        }
    }

    if (!valid)
	printf("Validation failed at i=%d\n", i);

    //printf(",N,NBytes,avg(usec),variance,FFT Rate [FFT/s],\n");
    printf("%d,%u,%lu,%lu,%.3f\n", size / UINT16_SIZE,
	   size, mean, var, 1000000.0f / mean);

    free(x);
    free(y);
    free(X);
    free(Y);
    free(tmp);
    return 0;
}
