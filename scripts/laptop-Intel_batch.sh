########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

if [[ $# -ne 2 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2] batch [28 56 112]"
    exit 1
fi

p=`nproc`
verbose=0
verbose=$1
batch=$2
loops=500

directory=../results/batch/laptop-Intel/$batch
mkdir -p $directory


#####################
###     CLFFT     ###
#####################

#### Intel & AMD platforms ####

for platform in Intel AMD;do
## CPU Device
cmd="./testCLFFTTimingsBatch.sh $verbose $platform version CPU -100 $loops $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFT.$platform.version.CPU.$loops.csv
## No Transfer
cmd="./testCLFFTTimingsBatchNoTransfer.sh $verbose $platform version CPU -100 $loops $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransfer.$platform.version.CPU.$loops.csv
## No Transform
cmd="./testCLFFTTimingsBatchNoTransform.sh $verbose $platform version CPU -100 $loops $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransform.$platform.version.CPU.$loops.csv
done
