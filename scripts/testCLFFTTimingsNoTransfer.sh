########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

# verbosity -> testType -> oclPlat -> oclVers -> oclDev -> FFT|iFFT -> sizeN -> warm -> nbLoop
# Call me with a verbosity=0 to display as csv format

if [[ $# -ne 6 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2] oclPlatform [Intel | AMD ] oclVersion [1.2 | 2.0] oclDevice [GPU | CPU] nbWarmup nbLoop"
    exit 1
fi

verbosity=0
oclPlatform="AMD"
oclVersion="2.0"
oclDevice="GPU"
nbWarmup=-100
nbLoop=1000

verbosity=$1
oclPlatform=$2
oclVersion=$3
oclDevice=$4
nbWarmup=$5
nbLoop=$6

binary="../clFFT/clFFT-2.6.1-GeeF/build/examples/examples/clfftAlternateNoTransfer"

for direction in -1 1;do
	echo "---------- FFT($direction) ----------"
		for N in 64 128 256 512 1024 1536 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576; do
			cmd="$binary $verbosity $oclPlatform $oclVersion $oclDevice $direction $N $nbWarmup $nbLoop"
        		if [ 0 != $verbosity ]; then
				echo $cmd
			fi
			$cmd
		done
	echo ""
done

