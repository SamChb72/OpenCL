########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

# verbosity -> testType -> oclPlat -> oclVers -> oclDev -> FFT|iFFT -> sizeN -> warm -> nbLoop
# Call me with a verbosity=0 to display as csv format

if [[ $# -ne 5 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2] oclPlatform [Intel | AMD ] oclDevice [GPU | CPU] nbWarmup nbLoop"
    exit 1
fi

verbosity=0
oclPlatform="AMD"
oclDevice="GPU"
nbWarmup=-100
nbLoop=1000

verbosity=$1
oclPlatform=$2
oclDevice=$3
nbWarmup=$4
nbLoop=$5

binary="../copyVector/copyOpenCL"

for N in 64 128 256 512 1024 1536 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576; do
	echo "---------- copy Latency($N ten tries) ----------"
		for globalSize in $N $N $N $N $N $N $N $N $N $N; do
			cmd="$binary 2 $verbosity $N $nbWarmup $nbLoop $globalSize $oclPlatform $oclDevice"
        		if [ 0 != $verbosity ]; then
				echo $cmd
			fi
			$cmd
		done
	echo ""
done

