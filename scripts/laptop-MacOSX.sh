########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

directory=../results/batch

if [[ $# -ne 1 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2]"
    exit 1
fi

p=4 
verbose=0
verbose=$1


#####################
###     FFTW3     ###
#####################

cmd="./testFFTW3Timings.sh $verbose -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.fftw.Intel.version.CPU.500.csv
# No Transfer
cmd="./testFFTW3TimingsNoTransfer.sh $verbose -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.fftwNoTransfer.Intel.version.CPU.500.csv
# No Transform
cmd="./testFFTW3TimingsNoTransform.sh $verbose -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.fftwNoTransform.Intel.version.CPU.500.csv



#####################
###      IPP      ###
#####################

cmd="./testIPPFFTTimings.sh $verbose -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.ipp.Intel.version.CPU.500.csv


#####################
###     CLFFT     ###
#####################

#### Intel platform ####

## GPU Device
cmd="./testCLFFTTimings.sh $verbose Intel version GPU -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.clFFT.Intel.version.GPU.500.csv
## No Transfer
cmd="./testCLFFTTimingsNoTransfer.sh $verbose Intel version GPU -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.clFFTNoTransfer.Intel.version.GPU.500.csv
## No Transform
cmd="./testCLFFTTimingsNoTransform.sh $verbose Intel version GPU -100 500"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.clFFTNoTransform.Intel.version.GPU.500.csv

#### Batch 14 ####
batch="14"
## GPU Device
cmd="./testCLFFTTimingsBatch.sh $verbose Intel version GPU -100 500 $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFT.Intel.version.GPU.500.csv
## No Transfer
cmd="./testCLFFTTimingsBatchNoTransfer.sh $verbose Intel version GPU -100 500 $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransfer.Intel.version.GPU.500.csv
## No Transform
cmd="./testCLFFTTimingsBatchNoTransform.sh $verbose Intel version GPU -100 500 $batch"
echo $cmd
$cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransform.Intel.version.GPU.500.csv

