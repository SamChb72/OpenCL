########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

directory=../results/desktop-kaveri-discrete
p=`nproc`

if [[ $# -ne 1 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2]"
    exit 1
fi

verbose=0
verbose=$1
loops=500

#####################
###     FFTW3     ###
#####################

cmd="./testFFTW3Timings.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftw.AMD.version.CPU.$loops.csv
# No Transfer
cmd="./testFFTW3TimingsNoTransfer.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftwNoTransfer.AMD.version.CPU.$loops.csv
# No Transform
cmd="./testFFTW3TimingsNoTransform.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftwNoTransform.AMD.version.CPU.$loops.csv


#####################
###      IPP      ###
#####################
source /opt/intel/ipp/bin/ippvars.sh intel64

cmd="./testIPPFFTTimings.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.ipp.AMD.version.CPU.$loops.csv



#####################
###     CLFFT     ###
#####################

#### CPU Platform ####
device=CPU
for platform in Intel AMD;do
    cmd="./testCLFFTTimings.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
    cmd="./testCLFFTTimingsNoTransfer.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
    cmd="./testCLFFTTimingsNoTransform.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clFFTNoTransform.$platform.version.$device.$loops.csv
done


## GPU Device
device=GPU
    cmd="./testCLFFTTimings.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
    cmd="./testCLFFTTimingsNoTransfer.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
    cmd="./testCLFFTTimingsNoTransform.sh $verbose $platform version $device -100 $loops"
    echo $cmd
    $cmd > $directory/core_$p.clFFTNoTransform.$platform.version.$device.$loops.csv

