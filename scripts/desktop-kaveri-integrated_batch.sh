########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

p=`nproc`

if [[ $# -ne 2 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2] batch [28 56 112]"
    exit 1
fi

verbose=0
verbose=$1
loops=500
batch=$2

directory=../results/batch/desktop-kaveri-integrated/$batch
mkdir -p $directory

#####################
###     CLFFT     ###
#####################

#### CPU Platform ####
device=CPU
for platform in Intel AMD;do
#    cmd="./testCLFFTTimings.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
#    cmd="./testCLFFTTimingsNoTransfer.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
#    cmd="./testCLFFTTimingsNoTransform.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clFFTNoTransform.$platform.version.$device.$loops.csv

    #batch
    cmd="./testCLFFTTimingsBatch.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
    cmd="./testCLFFTTimingsBatchNoTransfer.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
    cmd="./testCLFFTTimingsBatchNoTransform.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransform.$platform.version.$device.$loops.csv
done


## GPU Device
device=GPU
#    cmd="./testCLFFTTimings.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
#    cmd="./testCLFFTTimingsNoTransfer.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
#    cmd="./testCLFFTTimingsNoTransform.sh $verbose $platform version $device -100 $loops"
#    echo $cmd
#    $cmd > $directory/$(basename $0).core_$p.clFFTNoTransform.$platform.version.$device.$loops.csv

    #batch
    cmd="./testCLFFTTimingsBatch.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clfft.$platform.version.$device.$loops.csv
    ## No Transfer
    cmd="./testCLFFTTimingsBatchNoTransfer.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransfer.$platform.version.$device.$loops.csv
    ## No Transform
    cmd="./testCLFFTTimingsBatchNoTransform.sh $verbose $platform version $device -100 $loops $batch"
    echo $cmd
    $cmd > $directory/$(basename $0).core_$p.batch_$batch.clFFTNoTransform.$platform.version.$device.$loops.csv
