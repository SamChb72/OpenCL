########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

# verbosity -> FFT|iFFT -> sizeN -> warm -> nbLoop
# Call me with a verbosity=0 to display as csv format

if [[ $# -ne 3 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2] nbWarmup nbLoop"
    exit 1
fi

verbosity=0
nbWarmup=-100
nbLoop=1000

verbosity=$1
nbWarmup=$2
nbLoop=$3

binary="../fftw/fftwAlternateNoTransfer"

echo "$(basename $0) verbosity=$verbosity nbWarmup=$nbWarmup nbLoop=$nbLoop"

for direction in -1 1;do
	echo "---------- FFT($direction) ----------"
		for N in 64 128 256 512 1024 1536 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576; do
			cmd="$binary $verbosity $direction $N $nbWarmup $nbLoop"
			if [ $verbosity -ne 0 ]
			then
				echo $cmd
			fi
			$cmd
		done
	echo ""
done

