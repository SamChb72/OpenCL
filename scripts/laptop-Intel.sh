########################################################
#          Copyright (C) 2015 GreenFLOPS               #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

directory=../results/batch/laptop-Intel

if [[ $# -ne 1 ]] ; then
    echo "usage: $(basename $0) verbosity [0 .. 2]"
    exit 1
fi

p=4 
verbose=0
verbose=$1
loops=500

#####################
###     FFTW3     ###
#####################

cmd="./testFFTW3Timings.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftw.Intel.version.CPU.$loops.csv
# No Transfer
cmd="./testFFTW3TimingsNoTransfer.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftwNoTransfer.Intel.version.CPU.$loops.csv
# No Transform
cmd="./testFFTW3TimingsNoTransform.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.fftwNoTransform.Intel.version.CPU.$loops.csv




#####################
###      IPP      ###
#####################

cmd="./testIPPFFTTimings.sh $verbose -100 $loops"
echo $cmd
$cmd > $directory/core_$p.ipp.Intel.version.CPU.$loops.csv





#####################
###     CLFFT     ###
#####################

#### Intel & AMD platforms ####

for platform in Intel AMD;do
## CPU Device
cmd="./testCLFFTTimings.sh $verbose $platform version CPU -100 $loops"
echo $cmd
$cmd > $directory/core_$p.clFFT.$platform.version.CPU.$loops.csv
## No Transfer
cmd="./testCLFFTTimingsNoTransfer.sh $verbose $platform version CPU -100 $loops"
echo $cmd
$cmd > $directory/core_$p.clFFTNoTransfer.$platform.version.CPU.$loops.csv
## No Transform
cmd="./testCLFFTTimingsNoTransform.sh $verbose $platform version CPU -100 $loops"
echo $cmd
$cmd > $directory/core_$p.clFFTNoTransform.$platform.version.CPU.$loops.csv

done

