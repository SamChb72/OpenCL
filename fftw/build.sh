#!/bin/bash

gcc -O3 -o fftwAlternate fftwAlternate.c -I/opt/local/include -L/opt/local/lib -lfftw3
gcc -O3 -o fftwAlternateNoTransfer fftwAlternateNoTransfer.c -I/opt/local/include -L/opt/local/lib -lfftw3
gcc -O3 -o fftwAlternateNoTransform fftwAlternateNoTransform.c -I/opt/local/include -L/opt/local/lib -lfftw3

