#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <fftw3.h>

#define EXIT_FAILURE 1

#ifdef __APPLE__
#define CLOCK_PROCESS_CPUTIME_ID 1
//clock_gettime is not implemented on OSX
int clock_gettime(int clk_id, struct timespec *t)
{
    struct timeval now;
    int rv = gettimeofday(&now, NULL);

    if (rv)
	return rv;
    t->tv_sec = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}
#endif


int main(int argc, char *argv[])
{
    int i;
    fftw_complex *in;
    fftw_complex *in2;
    fftw_complex *out;
    fftw_plan plan_backward;
    fftw_plan plan_forward;

    //Set the size
    int N = 1024;
    int nb_loop = 1000;
    int verbose = 0;
    int warmup = 0;
    int dir = -1;

    if (5 > argc) {
	printf("Usage & exemples:\n");
	printf("%s $verbosity $direction $N $nbWarmup $nbLoop\n", argv[0]);
	printf("%s [0..2] [-1 1] 1024 -100 1000\n", argv[0]);
	exit(0);
    } else {
	verbose = atoi(argv[1]);
	dir = atoi(argv[2]);
	N = atoi(argv[3]);
	warmup = atoi(argv[4]);
	nb_loop = atoi(argv[5]);
    }



/*
  Create the input array.
*/
    in = fftw_malloc(sizeof(fftw_complex) * N);
    in2 = fftw_malloc(sizeof(fftw_complex) * N);


    for (i = 0; i < N; i++) {
	in[i][0] = 2.0 * i;
	in[i][1] = in[i][0] + 1.0;

	in2[i][1] = in2[i][0] = 1.0;
    }

    if (2 < verbose) {
	printf("\n");
	printf("  Input Data:\n");
	printf("\n");

	for (i = 0; i < N; i++) {
	    printf("  %3d  %12f  %12f\n", i, in[i][0], in[i][1]);
	}

	for (i = 0; i < N; i++) {
	    printf("  %3d  %12f  %12f\n", i, in2[i][0], in2[i][1]);
	}
    }
/*
  Create the output array.
*/
    out = fftw_malloc(sizeof(fftw_complex) * N);
    int run = 0;
    struct timeval utstart, utstop;
    double uduration;

    if (-1 == dir) {
	plan_forward =
	    fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    } else {
	plan_backward =
	    fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    }
    for (run = warmup; run < nb_loop; run++) {
	if (0 == run) {
	    gettimeofday(&utstart, NULL);
	}
	if (-1 == dir) {
	    if (0 == run % 2)
		plan_forward =
		    fftw_plan_dft_1d(N, in, out, FFTW_FORWARD,
				     FFTW_ESTIMATE);
	    else
		plan_forward =
		    fftw_plan_dft_1d(N, in2, out, FFTW_FORWARD,
				     FFTW_ESTIMATE);
#if 0
// NO TRANSFORM
	    fftw_execute(plan_forward);
#endif
	} else {
	    if (0 == run % 2)
		plan_backward =
		    fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD,
				     FFTW_ESTIMATE);
	    else
		plan_backward =
		    fftw_plan_dft_1d(N, in2, out, FFTW_BACKWARD,
				     FFTW_ESTIMATE);
#if 0
// NO TRANSFORM
	    fftw_execute(plan_backward);
#endif
	}
    }
    gettimeofday(&utstop, NULL);

    uduration =
	((utstop.tv_sec - utstart.tv_sec) * 1000000L + utstop.tv_usec) -
	utstart.tv_usec;

    if (2 < verbose) {
	printf("\n");
	printf("  Output FFT Coefficients:\n");
	printf("\n");

	for (i = 0; i < N; i++) {
	    printf("  %3d  %12f  %12f\n", i, out[i][0], out[i][1]);
	}
    }
    printf("%d, %f\n", N, uduration / nb_loop);

    fftw_free(in);
    fftw_free(in2);
    fftw_free(out);

    return 0;
}
