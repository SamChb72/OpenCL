/*******************************************************
*       Copyright (C) 2015 GreenFLOPS             *
*                                                      *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*                                                      *
*              contact@greenflops.com                  *
*******************************************************/


// Copy Kernel
__kernel void flop_0(__global short *in, __global short *out)
{
    int id = get_global_id(0);
    out[id] = in[id];		// 0 flop, just a copy
}


// 1 flop kernel
__kernel void flop_1(__global short *v)
{
    int i = get_global_id(0);
    v[i] -= 1;			// 1 flop
}
