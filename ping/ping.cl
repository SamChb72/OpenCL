__kernel void ping(__global int *A, __global int *C)
{                        
   // Get the work-item’s unique ID
   int idx = get_global_id(0);
   C[idx] = A[idx];
}
