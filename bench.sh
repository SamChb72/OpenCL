########################################################
#       Copyright (C) 2015 GreenFLOPS             #
#						       #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#						       #
#              contact@greenflops.com                  #
########################################################

#!/bin/bash

if [ "HSA" = "$1" ]; then
	info="              HSA device                 "
	prog="./benchHSA"
	fft="./fftHSA"
else if [ "OpenCL" = "$1" ]; then
	info="            OpenCL device                "
	prog="./benchOpenCL"
	fft="./fftOpenCL"
	fi
fi

echo "-----------------------------------------"
echo "${info}"
echo "         4 Bytes float Vector            "
echo " AVG and Variance from 1000 measurements "
echo "-----------------------------------------"


echo "Radix2 FFT"
echo "N,bytes,avg[usec],sigma2,FFT Rate [FFT/s]"
for stages in 3 4 5 6 7 8 9 10 11 12 13 14 15 16; do
	cmd="$fft $stages"
	#echo $cmd
	$cmd
done
echo ""

echo "Vector COPY"
echo "WITH WARMUP 100 loops"
echo "N,bytes,avg[usec],sigma2,Copy rate [vector/s]"
for n in 1 10 100 1kB 10kB 100kB 1MB 10MB 50MB 100MB; do
	cmd="$prog -flops0 warmup 100 $n"
	#echo $cmd
	$cmd
done
echo ""

echo "Vector COPY"
echo "WITHOUT WARMUP"
echo "N,bytes,avg[usec],sigma2,Copy rate [vector/s]"
for n in 1 10 100 1kB 10kB 100kB 1MB 10MB 50MB 100MB; do
	cmd="$prog -flops0 $n"
	#echo $cmd
	$cmd
done
echo ""

echo "One arithmetic operation (-) per kernel launch"
echo "WITH WARMUP 100 loops"
echo "N,bytes,avg[usec],sigma2,Compute speed [Gflops]"
for n in 1 10 100 1kB 10kB 100kB 1MB 10MB 50MB 100MB; do
	cmd="$prog -flops1 warmup 100 $n"
	#echo $cmd
	$cmd
done
echo ""

echo "One arithmetic operation (-) per kernel launch"
echo "WITHOUT WARMUP"
echo "N,bytes,avg[usec],sigma2,Compute speed [Gflops]"
for n in 1 10 100 1kB 10kB 100kB 1MB 10MB 50MB 100MB; do
	cmd="$prog -flops1 $n"
	#echo $cmd
	$cmd
done
echo ""

