#include <sys/time.h>
#include "ippcore.h"
#include "ipps.h"
#include "ippi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rdtsc.h"
#include <time.h>


#define EXIT_FAILURE 1

#ifdef __APPLE__
#define CLOCK_PROCESS_CPUTIME_ID 1
//clock_gettime is not implemented on OSX
int clock_gettime(int clk_id, struct timespec *t)
{
    struct timeval now;
    int rv = gettimeofday(&now, NULL);

    if (rv)
       return rv;
    t->tv_sec = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
return 0;                                }
#endif                             

int main(int argc, char *argv[])
{
    //Set the size
    int N = 1024;
    int nb_loop = 1000;
    int verbose = 0;
    int warmup = 0;
    int dir = -1;


    if (5 > argc) {
	printf("Usage & exemples:\n");
	printf("%s $verbosity $direction $N $nbWarmup $nbLoop\n", argv[0]);
	printf("%s [0..2] [-1 1] 1024 -100 1000\n", argv[0]);
	exit(0);
    } else {
	verbose = atoi(argv[1]);
	dir = atoi(argv[2]);
	N = atoi(argv[3]);
	warmup = atoi(argv[4]);
	nb_loop = atoi(argv[5]);
    }

    Ipp32fc *Src = (Ipp32fc *) malloc(N * sizeof(*Src));
    Ipp32fc *Src2 = (Ipp32fc *) malloc(N * sizeof(*Src2));
    int print_iter = 0;
    while (print_iter < N) {
	float x = print_iter * 2;
	float y = x + 1;
	Src[print_iter].re = x;
	Src[print_iter].im = y;
	//printf("(%f, %f) ", x, y);
	if (2<verbose)
	    printf("%f+%fi,", Src[print_iter].re, Src[print_iter].im);

	// SRC2
	Src2[print_iter].re = Src2[print_iter].im = 1;
	if (2<verbose)
	    printf("%f+%fi,", Src2[print_iter].re, Src2[print_iter].im);
	print_iter++;
    }
	if (2<verbose)printf("\n");


    Ipp32fc *Dst = (Ipp32fc *) malloc(N * sizeof(*Dst));
    Ipp32fc *Dst2 = (Ipp32fc *) malloc(N * sizeof(*Dst2));

    int FFTOrder = log(N) / log(2);
    IppsFFTSpec_C_32fc *pSpec = 0;

    Ipp8u *pMemSpec = 0;
    Ipp8u *pMemInit = 0;
    Ipp8u *pMemBuffer = 0;

    int sizeSpec = 0;
    int sizeInit = 0;
    int sizeBuffer = 0;

//    int flag = IPP_FFT_NODIV_BY_ANY;
    int flag = IPP_FFT_DIV_INV_BY_N;

    /// get sizes for required buffers
    ippsFFTGetSize_C_32fc(FFTOrder, flag, ippAlgHintNone, &sizeSpec,
			  &sizeInit, &sizeBuffer);

    /// allocate memory for required buffers
    pMemSpec = (Ipp8u *) ippMalloc(sizeSpec);

    if (sizeInit > 0) {
	pMemInit = (Ipp8u *) ippMalloc(sizeInit);
    }

    if (sizeBuffer > 0) {
	pMemBuffer = (Ipp8u *) ippMalloc(sizeBuffer);
    }
    /// initialize FFT specification structure
    ippsFFTInit_C_32fc(&pSpec, FFTOrder, flag, ippAlgHintNone, pMemSpec,
		       pMemInit);

    /// free initialization buffer
    if (sizeInit > 0) {
	ippFree(pMemInit);
    }
// Do the 1st FFT
    {
	int run = 0;
	struct timeval utstart, utstop;
	double uduration;

	for (run = warmup; run < nb_loop; run++) {
	    if (0 == run) {
		gettimeofday(&utstart, NULL);
	    }
	    if (-1 == dir) {
		/// perform Forward FFT
		if (0 == run % 2)
		    ippsFFTFwd_CToC_32fc(Src, Dst, pSpec, pMemBuffer);
		else
		    ippsFFTFwd_CToC_32fc(Src2, Dst2, pSpec, pMemBuffer);
	    } else {
		/// perform Backward FFT
		if (0 == run % 2)
		    ippsFFTInv_CToC_32fc(Src, Dst, pSpec, pMemBuffer);
		else
		    ippsFFTInv_CToC_32fc(Src2, Dst2, pSpec, pMemBuffer);

	    }
	}
	gettimeofday(&utstop, NULL);

	uduration = ((utstop.tv_sec - utstart.tv_sec) * 1000000L + utstop.tv_usec) - utstart.tv_usec;

	if (2<verbose) {
	    print_iter = 0;
	    while (print_iter < N) {
		printf("%f+%fi,", Dst[print_iter].re, Dst[print_iter].im);
		print_iter++;
	    }
	    printf("\n");
	}

	printf("%f\n", uduration / nb_loop);
    }

/// free buffers
    if (sizeBuffer > 0) {
	ippFree(pMemBuffer);
    }

    ippFree(pMemSpec);
}
