#include <sys/time.h>
#include "ippcore.h"
#include "ipps.h"
#include "ippi.h"

#define NB_MEASURE 1000

int main(int argc, char *argv[])
{
    //Set the size
    int N=1024;
if (2 > argc) {
       printf("Usage & exemples:\n");
        printf("./fftIPP size\n");
        printf("./fftIPP 1024\n");
} else
{
	N = atoi(argv[1]);
}


    // Spec and working buffers
    IppsDFTSpec_C_16fc *pDFTSpec=0;
    Ipp8u  *pDFTInitBuf, *pDFTWorkBuf;

    // Allocate complex buffers
    Ipp16fc *pSrc=ippsMalloc_16fc(N);
    Ipp16fc *pDst=ippsMalloc_16fc(N); 

    // Query to get buffer sizes
    int sizeDFTSpec,sizeDFTInitBuf,sizeDFTWorkBuf;
    ippsDFTGetSize_C_16fc(N, IPP_FFT_NODIV_BY_ANY, 
        ippAlgHintAccurate, &sizeDFTSpec, &sizeDFTInitBuf, &sizeDFTWorkBuf);

    // Alloc DFT buffers
    pDFTSpec    = (IppsDFTSpec_C_16fc*)ippsMalloc_8u(sizeDFTSpec);
    pDFTInitBuf = ippsMalloc_8u(sizeDFTInitBuf);
    pDFTWorkBuf = ippsMalloc_8u(sizeDFTWorkBuf);

    // Initialize DFT
    ippsDFTInit_C_16fc(N, IPP_FFT_NODIV_BY_ANY, 
        ippAlgHintAccurate, pDFTSpec, pDFTInitBuf);
    if (pDFTInitBuf) ippFree(pDFTInitBuf);

    // Do the DFT
    int run = 0;
    struct timeval tstart, tstop;
float meas;
gettimeofday(&tstart, NULL);
    for (run = 0; run < NB_MEASURE; run++) {
    ippsDFTFwd_CToC_16fc(pSrc,pDst,pDFTSpec,pDFTWorkBuf);
	}
gettimeofday(&tstop, NULL);
meas =((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) - tstart.tv_usec;

printf("meas=%.3fusec\n", meas/NB_MEASURE);

    //check results
    ippsDFTInv_CToC_16fc(pDst,pDst,pDFTSpec,pDFTWorkBuf);
    int OK=1;
	int i;
    for (i=0;i<N;i++) {
        pDst[i].re/=(Ipp16f)N;
        pDst[i].im/=(Ipp16f)N;
        if ((abs(pSrc[i].re-pDst[i].re)>.001) || 
            (abs(pSrc[i].im-pDst[i].im)>.001) ) 
        {
            OK=0;break;
        }
    }
    puts(OK==1?"DFT OK":"DFT Fail");

    if (pDFTWorkBuf) ippFree(pDFTWorkBuf);
    if (pDFTSpec) ippFree(pDFTSpec);

    ippFree(pSrc);
    ippFree(pDst);
}
