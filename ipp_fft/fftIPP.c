#include <sys/time.h>
#include "ippcore.h"
#include "ipps.h"
#include "ippi.h"
#include <stdio.h>
#include "rdtsc.h"
#include <time.h>

#define NB_MEASURE 1000

#define EXIT_FAILURE 1

int main(int argc, char *argv[])
{
    //Set the size
    int N = 1024;
    int nb_loop = NB_MEASURE;
    int verbose = 0;

    if (4 > argc) {
	printf("Usage & exemples:\n");
	printf("./fftIPP verbose nb_loop size\n");
	printf("./fftIPP 0 1000 1024\n");
    } else {
	verbose = atoi(argv[1]);
	nb_loop = atoi(argv[2]);
	N = atoi(argv[3]);
    }

    Ipp32fc *Src = (Ipp32fc *) malloc(N * sizeof(*Src));
    Ipp32fc *Src2 = (Ipp32fc *) malloc(N * sizeof(*Src2));
    int print_iter = 0;
    while (print_iter < N) {
	float x = print_iter * 2;
	float y = x + 1;
	Src[print_iter].re = x;
	Src[print_iter].im = y;
	//printf("(%f, %f) ", x, y);
	if (verbose)
	    printf("%f+%fi,", Src[print_iter].re, Src[print_iter].im);

	// SRC2
	Src2[print_iter].re = Src2[print_iter].im = 1;
	if (verbose)
	    printf("%f+%fi,", Src2[print_iter].re, Src2[print_iter].im);
	print_iter++;
    }

    printf("\n");

    Ipp32fc *Dst = (Ipp32fc *) malloc(N * sizeof(*Dst));
    Ipp32fc *Dst2 = (Ipp32fc *) malloc(N * sizeof(*Dst2));

    int FFTOrder = log(N) / log(2);
    IppsFFTSpec_C_32fc *pSpec = 0;

    Ipp8u *pMemSpec = 0;
    Ipp8u *pMemInit = 0;
    Ipp8u *pMemBuffer = 0;

    int sizeSpec = 0;
    int sizeInit = 0;
    int sizeBuffer = 0;

    int flag = IPP_FFT_NODIV_BY_ANY;

    /// get sizes for required buffers
    ippsFFTGetSize_C_32fc(FFTOrder, flag, ippAlgHintNone, &sizeSpec,
			  &sizeInit, &sizeBuffer);

    /// allocate memory for required buffers
    pMemSpec = (Ipp8u *) ippMalloc(sizeSpec);

    if (sizeInit > 0) {
	pMemInit = (Ipp8u *) ippMalloc(sizeInit);
    }

    if (sizeBuffer > 0) {
	pMemBuffer = (Ipp8u *) ippMalloc(sizeBuffer);
    }
    /// initialize FFT specification structure
    ippsFFTInit_C_32fc(&pSpec, FFTOrder, flag, ippAlgHintNone, pMemSpec,
		       pMemInit);

    /// free initialization buffer
    if (sizeInit > 0) {
	ippFree(pMemInit);
    }
// Do the 1st FFT
    {
	int run = 0;
	struct timeval tstart, tstop;
	float meas;
	struct timespec start, stop;
	double accum;

// CLOCK_REALTIME
// CLOCK_THREAD_CPUTIME_ID
// CLOCK_PROCESS_CPUTIME_ID
	gettimeofday(&tstart, NULL);
	if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start) == -1) {
	    perror("clock gettime");
	    return EXIT_FAILURE;
	}
	for (run = 0; run < nb_loop; run++) {
	    /// perform forward FFT
	    ippsFFTFwd_CToC_32fc(Src, Dst, pSpec, pMemBuffer);
	}
	if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop) == -1) {
	    perror("clock gettime");
	    return EXIT_FAILURE;
	}
	gettimeofday(&tstop, NULL);

	accum = (double) (stop.tv_nsec - start.tv_nsec);
	accum /= (double) nb_loop;

	meas =
	    ((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) -
	    tstart.tv_usec;

	if (verbose) {
	    print_iter = 0;
	    while (print_iter < N) {
		printf("%f+%fi,", Dst[print_iter].re, Dst[print_iter].im);
		print_iter++;
	    }
	}

	printf("\n0+i, 1+2i, ...\n");
	printf("%9.9f ns\n", accum);
	printf("meas=%9.9f usec \n", meas / nb_loop);
    }

// Do the 2d FFT
    {
	int run = 0;
	struct timeval tstart, tstop;
	float meas;
	struct timespec start, stop;
	double accum;

// CLOCK_REALTIME
// CLOCK_THREAD_CPUTIME_ID
// CLOCK_PROCESS_CPUTIME_ID
	gettimeofday(&tstart, NULL);
	if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start) == -1) {
	    perror("clock gettime");
	    return EXIT_FAILURE;
	}
	for (run = 0; run < nb_loop; run++) {
	    /// perform forward FFT
	    ippsFFTFwd_CToC_32fc(Src2, Dst2, pSpec, pMemBuffer);
	}
	if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop) == -1) {
	    perror("clock gettime");
	    return EXIT_FAILURE;
	}
	gettimeofday(&tstop, NULL);

	accum = (double) (stop.tv_nsec - start.tv_nsec);
	accum /= (double) nb_loop;

	meas =
	    ((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) -
	    tstart.tv_usec;
	if (verbose) {
	    print_iter = 0;
	    while (print_iter < N) {
		printf("%f+%fi,", Dst2[print_iter].re,
		       Dst2[print_iter].im);
		print_iter++;
	    }
	}
	printf("\n1+i, 1+i, ...\n");
	printf("%9.9f ns\n", accum);
	printf("meas=%9.9f usec \n", meas / nb_loop);
    }

/// free buffers
    if (sizeBuffer > 0) {
	ippFree(pMemBuffer);
    }

    ippFree(pMemSpec);
}
