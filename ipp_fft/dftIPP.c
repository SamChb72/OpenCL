#include <sys/time.h>
#include "ippcore.h"
#include "ipps.h"
#include "ippi.h"
#include <stdio.h>
#include "rdtsc.h"

#define NB_MEASURE 1000

int main(int argc, char *argv[])
{
    //Set the size
    int N=1024;
int nb_loop=NB_MEASURE;

if (3 > argc) {
       printf("Usage & exemples:\n");
        printf("./fftIPP nb_loop size\n");
        printf("./fftIPP 1000 1024\n");
} else
{
	nb_loop= atoi(argv[1]);
	N = atoi(argv[2]);
}


    // Spec and working buffers
    IppsDFTSpec_C_32fc *pDFTSpec=0;
    Ipp8u  *pDFTInitBuf, *pDFTWorkBuf;

    // Allocate complex buffers
    Ipp32fc *pSrc=ippsMalloc_32fc(N);
    Ipp32fc *pDst=ippsMalloc_32fc(N); 

    // Query to get buffer sizes
    int sizeDFTSpec,sizeDFTInitBuf,sizeDFTWorkBuf;
    ippsDFTGetSize_C_32fc(N, IPP_FFT_NODIV_BY_ANY, 
        ippAlgHintAccurate, &sizeDFTSpec, &sizeDFTInitBuf, &sizeDFTWorkBuf);

    // Alloc DFT buffers
    pDFTSpec    = (IppsDFTSpec_C_32fc*)ippsMalloc_8u(sizeDFTSpec);
    pDFTInitBuf = ippsMalloc_8u(sizeDFTInitBuf);
    pDFTWorkBuf = ippsMalloc_8u(sizeDFTWorkBuf);

    // Initialize DFT
    ippsDFTInit_C_32fc(N, IPP_FFT_NODIV_BY_ANY, 
        ippAlgHintAccurate, pDFTSpec, pDFTInitBuf);
    if (pDFTInitBuf) ippFree(pDFTInitBuf);

    // Do the DFT
    int run = 0;
    struct timeval tstart, tstop;
float meas;
  unsigned long long a,b, c, d;

gettimeofday(&tstart, NULL);
a = rdtsc();
    for (run = 0; run < nb_loop; run++) {
	c = rdtsc();
    ippsDFTFwd_CToC_32fc(pSrc,pDst,pDFTSpec,pDFTWorkBuf);
	d = rdtsc();
	}
b = rdtsc();
gettimeofday(&tstop, NULL);
meas =((tstop.tv_sec - tstart.tv_sec) * 1000000L + tstop.tv_usec) - tstart.tv_usec;

printf("meas=%.3fusec, outsideloop=%llu, lastinsideloop=%llu\n", meas/nb_loop, (b-a)/nb_loop, (d-c));

    //check results
    ippsDFTInv_CToC_32fc(pDst,pDst,pDFTSpec,pDFTWorkBuf);
    int OK=1;
	int i;
    for (i=0;i<N;i++) {
        pDst[i].re/=(Ipp32f)N;
        pDst[i].im/=(Ipp32f)N;
        if ((abs(pSrc[i].re-pDst[i].re)>.001) || 
            (abs(pSrc[i].im-pDst[i].im)>.001) ) 
        {
            OK=0;break;
        }
    }
    puts(OK==1?"DFT OK":"DFT Fail");

    if (pDFTWorkBuf) ippFree(pDFTWorkBuf);
    if (pDFTSpec) ippFree(pDFTSpec);

    ippFree(pSrc);
    ippFree(pDst);
}
