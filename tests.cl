__kernel void test(__global short *v)
{
    int i = get_global_id(0);
    short a, b, c, d;
    if (!(i % 4)) {
	a = 2 * v[i] + 1;
	b = 2 * v[i + 1] + 2;
	c = 3 * v[i + 2] + 3;
	d = 4 * v[i + 3] + 4;

	v[i] = a + b + c + d;
    }
}
