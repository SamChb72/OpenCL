/*******************************************************
*       Copyright (C) 2015 GreenFLOPS	       *
*						       *
* This file is part of GreenFLOPS projects and can not *
* be copied and/or distributed in any medium or format *
*    without the express permission of GreenFLOPS      *
*						       *
*              contact@greenflops.com		       *
*******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>
#include <limits.h>

#define NB_MEASURE 100
#define NANOINUSEC 1000

#define DEBUG 1
#undef DEBUG

void flop_0(short *u, short *v, unsigned long int length)
{
    unsigned long int i;
    for (i = 0; i < length; i++) {
	v[i] = u[i];
    }
}

void flop_1(short *v, unsigned long int length)
{
    unsigned long int i;
    for (i = 0; i < length; i++) {
	v[i] -= 1;
    }
}

int main(int argc, char *argv[])
{
    struct timeval tstart, tstop;
    unsigned long int meas[NB_MEASURE];
    unsigned long int mean;
    unsigned long int var;
    unsigned long int length;
    unsigned long int i;
    int run;
    short *x, *y;
    int test = 0;

    if (argc < 3) {
	printf("Usage: ./tests type length\n");
	length = 2 * 1000 * 1000;
	test = 0;
	printf("Ex: ./tests 0 %lu\n", length);
    } else {
	test = atoi(argv[1]);
	length = 2 * atoi(argv[2]);
    }

    x = (short *) calloc(length, sizeof(short));
    y = (short *) calloc(length, sizeof(short));

    for (i = 0; i < length; i++) {
	x[i] = 1;
	//printf("x[%lu]=%lu\n", i, x[i]);
    }

    for (run = -100; run < NB_MEASURE; run++) {
	int nbtest = 0;
	if (run >= 0)
	    gettimeofday(&tstart, NULL);
	for (nbtest = 0; nbtest < NANOINUSEC; nbtest++) {
	    if (0 == test)
		flop_0(x, y, length);
	    else if (1 == test)
		flop_1(x, length);
	}
	if (run >= 0) {
	    gettimeofday(&tstop, NULL);
	    meas[run] =
		((tstop.tv_sec - tstart.tv_sec) * 1000000L +
		 tstop.tv_usec) - tstart.tv_usec;
	    meas[run];
	}
    }

    for (run = 0; run < NB_MEASURE; run++) {
	mean += meas[run];
    }

    mean /= NB_MEASURE;
    for (run = 0; run < NB_MEASURE; run++) {
	var += ((meas[run] - mean) * (meas[run] - mean));
    }
    var /= NB_MEASURE;

    for (i = 0; i < 10; i++)
	printf("x[%lu] = %d\t", i, x[i]);

    printf("\nAVG=%dnsec, VAR=%d\n", (int) mean, (int) var);

    return 0;
}
